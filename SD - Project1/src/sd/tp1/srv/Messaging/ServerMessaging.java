package sd.tp1.srv.Messaging;

import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class ServerMessaging  {
	
	private Properties props;
	private KafkaProducer<String, String> producer;
	
	public ServerMessaging() {
		
		Properties env = System.getProperties();
		props = new Properties();
		
		props.put("zk.connect", env.getOrDefault("zk.connect", "localhost:2181/"));
		props.put("bootstrap.servers", env.getOrDefault("bootstrap.servers", "localhost:9092"));
		props.put("log.retention.ms", 1000);

		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("key.serializer", StringSerializer.class.getName());
		props.put("value.serializer", StringSerializer.class.getName());

		producer = new KafkaProducer<>(props);
		
	}
	
	
	public void addUpdatedAlbum(String album) {
		ProducerRecord<String, String> data = new ProducerRecord<>("updated" , album);
		producer.send(data);
	}
	

}
