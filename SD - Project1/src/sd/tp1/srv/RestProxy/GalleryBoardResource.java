package sd.tp1.srv.RestProxy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.NameNotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import sd.tp1.srv.InexistentAlbumException;
import sd.tp1.srv.Messaging.ServerMessaging;

@Path("/Gallery")
public class GalleryBoardResource {
	
	// Substituir pela API key atribuida
	final String apiKey = "0f6d08ea6851131";
	// Substituir pelo API secret atribuido
	final String apiSecret = "e0f723612ddbea61cda8c70a5002cccfd700f569";	
	//go here for new code
	//https://api.imgur.com/oauth2/authorize?client_id=0f6d08ea6851131&response_type=pin 
	protected static String code = "f1aef4c59f";
	protected static String user = "JPB18";
		
	protected static OAuth20Service service;
	protected static OAuth2AccessToken accessToken;
	
	static File directory = new File(".", "Gallery");
	static ServerMessaging svmsg = new ServerMessaging();
	static Map<String, String> idMatching = new HashMap<String, String>();
	static Map<String, String> nameMatching = new HashMap<String, String>();
	
	private String getTitle(String id) throws ParseException {
		OAuthRequest request = new OAuthRequest(Verb.GET, 
				"https://api.imgur.com/3/account/" + user + "/album/" + id, service);
		service.signRequest(accessToken, request);
		Response res = request.send();
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(res.getBody());
		JSONObject data = (JSONObject) obj.get("data");
		return (String) data.get("title");
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response getAlbums() {
		
		System.out.println("Getting albums...");
		
		if(!directory.exists())
			directory.mkdir();
		
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				"https://api.imgur.com/3/account/" + user + "/albums/ids", service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		//System.out.println(albumsRes.getCode());

		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		List<String> list = new ArrayList<String>(albums.size());
		while (albumsIt.hasNext()) {
			try {
				String id = albumsIt.next().toString();
				String title = getTitle(id);
				this.idMatching.put(id, title);
				this.nameMatching.put(title, id);
				list.add(title);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(list.isEmpty()) {
			System.out.println("None found...");
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		} else { 
			System.out.println("Found " + list.size() + " albuns");
			return javax.ws.rs.core.Response.ok(list).build();
		}
		
	}

	/**
	 * Creates a new album if one with said name doesn't exist
	 * @param name Name of the album to create
	 * @return Status.OK if successful, Status.PRECONDITION_FAILED if the album already exists
	 * @custom.pre the album doesn't exist 
	 */
	@POST
	@Path("/CreateAlbum")
	public javax.ws.rs.core.Response createAlbum(String name)  {
		if(!directory.exists())
			directory.mkdir();
		
		String url = String.format("https://api.imgur.com/3/album?title=%s", name);
		OAuthRequest albumsReq = new OAuthRequest(Verb.POST,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		String succ = (String) res.get("success");
		boolean isSuccess = Boolean.getBoolean(succ);
		
		if(isSuccess) {
			String id = (String)res.get("id");
			this.idMatching.put(id, name);
			this.nameMatching.put(name, id);
			//svmsg.addUpdatedAlbum(name);
			return javax.ws.rs.core.Response.ok().build();
		} else {
			return javax.ws.rs.core.Response.status(Status.PRECONDITION_FAILED).build();
		}
		
		
		
	}

	
	private String getAlbumId(String name) throws NameNotFoundException {
		
		System.out.println("Getting " + name + " id.");
		
		String id = nameMatching.get(name);
		//if it returns null, we'll force the server to update the album info...
		if(id == null) {
			System.out.println("We don't have the album, fetching new list...");
			this.getAlbums();
			id = nameMatching.get(name);
			if(id == null) { //if after updating, it's null return not found
				System.out.println("Haven't found yet, throwing exception...");
				throw new NameNotFoundException();
			}
		}
		System.out.println("ID: " + name);
		return id;
	}
	
	/**
	 * Removes an album with said name, if it exists
	 * @param name name of the album to remove
	 * @return OK if sucessful, NOT_FOUND if the album doesnt exist
	 * @custom.pre album exists
	 */
	@DELETE
	@Path("/DeleteAlbum/{name}")
	public javax.ws.rs.core.Response deleteAlbum(@PathParam("name") String name) {
		
		if(!directory.exists())
			directory.mkdir();

		
		String id;
		try {
			id = getAlbumId(name);
		} catch (NameNotFoundException e1) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		String url = String.format("https://api.imgur.com/3/album/%s", id);
		OAuthRequest albumsReq = new OAuthRequest(Verb.DELETE,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		boolean isSuccess = (boolean) res.get("success");
		
		if(isSuccess) {
			//svmsg.addUpdatedAlbum(name);
			System.out.println("Sucessfully deleted album " + name);
			return javax.ws.rs.core.Response.ok().build();
		} else {
			System.out.println("Failed to delete album " + name);
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Returns a list of picture names
	 * @param album album where the pictures are located
	 * @return OK with list or NOT_FOUND if the album doesnt exist or the album is empty
	 */
	@GET
	@Path("/ListPictures/{album}")
	@Produces(MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response getListOfPictures(@PathParam("album") String album) {
		
		if(!directory.exists())
			directory.mkdir();
		
		String id;
		try {
			id = getAlbumId(album);
		} catch (NameNotFoundException e1) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
		String url = String.format("https://api.imgur.com/3/album/%s/images", id);
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		List<String> list = new ArrayList<String>(albums.size());
		while (albumsIt.hasNext()) {
			JSONObject elem = (JSONObject) albumsIt.next();
			String name = (String) elem.get("name");
			list.add(name);
		}

		boolean isSuccess = (boolean) res.get("success");		
		if(isSuccess) {
			return javax.ws.rs.core.Response.ok(list).build();
		} else {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
				
		
	}
	
	
	/**
	 * Returns a data of a picture.
	 * @param album
	 * @param picture
	 * @return OK with data or NOT_FOUND if album or picture doesnt exist.
	 * @throws IOException
	 */
	@GET
	@Path("/get/{album}/{picture}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public javax.ws.rs.core.Response getPictureData(@PathParam("album") String album, @PathParam("picture") String picture) throws IOException {
		
		
		try {
			System.out.println("Getting picture data.");
			
			if(!directory.exists())
				directory.mkdir();
			
			String albumId;
			try {
				albumId = getAlbumId(album);
				//System.out.println(album + " id is " + albumId);
			} catch (NameNotFoundException e) {
				//System.out.println("Couldn't find album " + album);
				return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build(); 
			}
			
			String url = String.format("https://api.imgur.com/3/album/%s/images", albumId);
			OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
					url, service);
			service.signRequest(accessToken, albumsReq);
			final Response albumsRes = albumsReq.send();
			
			JSONParser parser = new JSONParser();
			JSONObject res;
			res = (JSONObject) parser.parse(albumsRes.getBody());
			
			//if the call failed return not found
			boolean success = (boolean)res.get("success"); 
			if(!success) {
				System.out.println("Request wasn't sucessfull");
				return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
			} else {
				System.out.println("Query was successfull");
			}
			
			JSONArray albums = (JSONArray) res.get("data");
			Iterator albumsIt = albums.iterator();
			long size = 0;
			url = null;
			while (albumsIt.hasNext() && url == null) {
				JSONObject elem = (JSONObject) albumsIt.next();
				String name = (String) elem.get("name");
				if(name.equals(picture)) {
					url = (String)elem.get("link");
					size = (long)elem.get("size");
				}
			}
			
			System.out.println("URL is " + url + " and size is " + size);
			
			URL link = new URL(url);
			System.out.println("Getting image @ " + url);
			InputStream stream = link.openStream();
			byte[] data = new byte[(int) size];
			int totalRead = 0;
			int read = 0;
			while((read = stream.read(data, totalRead, data.length - totalRead)) != -1) {
				System.out.println("Read: " + read);
				totalRead += read;
			}
			
	
			//System.out.println("Read " + totalRead + " bytes out of " + size + ". Returning it...");
			return javax.ws.rs.core.Response.ok(data).build();
				
		} catch (Exception e) {
			e.printStackTrace();
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
	}

	
	/**
	 * uploads a picture
	 * @param album
	 * @param name
	 * @param data
	 * @return OK uploads the picture or NOT_FOUND if album or picture doesnt exist.
	 */
	@POST
	@Path("/Upload/{album}/{name}")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public javax.ws.rs.core.Response uploadPicture(@PathParam("album") String album, @PathParam("name") String name, byte[] data){
		
		//System.out.println("Recieved picture");
		if(!directory.exists())
			directory.mkdir();
		
		String id;	
		try {
			id = getAlbumId(album);
		} catch (NameNotFoundException e1) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		String url = String.format("https://api.imgur.com/3/upload");
		OAuthRequest albumsReq = new OAuthRequest(Verb.POST,
				url, service);
			albumsReq.addPayload(data);
			albumsReq.addQuerystringParameter("album", id);
			albumsReq.addQuerystringParameter("name", name);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
		
		boolean isSuccess = (boolean)res.get("success");
		
		if(isSuccess) {
			
			//svmsg.addUpdatedAlbum(album);
			System.out.println("Successfully updated picture " + name + " @ " + album);			
			return javax.ws.rs.core.Response.ok().build();
		} else {
			System.out.println("Fail");
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String getPictureId(String albumId, String name) throws InexistentAlbumException, NameNotFoundException, ParseException {
		String url = String.format("https://api.imgur.com/3/album/%s/images", albumId);
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res = (JSONObject) parser.parse(albumsRes.getBody());
		
		//if the call failed return not found
		boolean success = (boolean)res.get("success"); 
		if(!success) {
			throw new InexistentAlbumException("Server request unsuccessfull: " + ((int)res.get("status")));
		} 
		
		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		while (albumsIt.hasNext()) {
			JSONObject elem = (JSONObject) albumsIt.next();
			String n = (String) elem.get("name");
			if(name.equals(n)) {
				return (String) elem.get("id");
			}
		}

		throw new NameNotFoundException("Name " + name + " doesn't exist.");
		
	}

	/**
	 * Deletes a picture.
	 * @param album
	 * @param picture
	 * @return OK if successful or NOT_FOUND if album or picture doesnt exist.
	 */
	@DELETE
	@Path("/DeletePicture/{album}/{picture}")
	public javax.ws.rs.core.Response deletePicture(@PathParam("album")String album, @PathParam("picture")String picture){
		if(!directory.exists())
			directory.mkdir();
		
		System.out.println("Deleting picture " + picture + " @ " + album);
		
		String idAlbum, idPicture;
		try {
			idAlbum = getAlbumId(album);
			idPicture = getPictureId(idAlbum, picture);			
		} catch (NameNotFoundException | InexistentAlbumException | ParseException e) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		String url = String.format("https://api.imgur.com/3/image/%s", idPicture);
		OAuthRequest albumsReq = new OAuthRequest(Verb.DELETE,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		boolean isSuccess = (boolean) res.get("success");
		
		if(isSuccess) {
			System.out.println("Picture deleted");
			//svmsg.addUpdatedAlbum(album);
			return javax.ws.rs.core.Response.ok().build();
		} else {
			System.out.println("Failed to delete");
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
	}

	
	
	public static final String PERMISSION_TYPE = "Access-Control-Allow-Origin";
	public static final String PERMISSION = "*";
		
	@GET
	@Path("/lastModified/{album}/{picture}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public javax.ws.rs.core.Response getPictureLastChange(@PathParam("album")String album, @PathParam("picture")String picture) {
		String idAlbum, idPicture;
		try {
			idAlbum = getAlbumId(album);
			idPicture = getPictureId(idAlbum, picture);			
		} catch (NameNotFoundException | InexistentAlbumException | ParseException e) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
		String url = String.format("https://api.imgur.com/3/image/%s", idPicture);
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();	
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
		
		boolean isSuccess = (boolean) res.get("success");
		if(isSuccess) {
			try {
				long timestamp = (long) res.get("datetime");			
				return javax.ws.rs.core.Response.ok(timestamp).build();
			} catch (Exception ex) {
				ex.printStackTrace();
				
				return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
			}
		} else {
			return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
		}
			
		
		
		
	}
	

}
