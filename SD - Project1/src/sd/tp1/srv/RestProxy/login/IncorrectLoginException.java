package sd.tp1.srv.RestProxy.login;

public class IncorrectLoginException extends Exception{
	private static final long serialVersionUID = 1L;

	public IncorrectLoginException(String message) {
		super(message);
	}

}
