package sd.tp1.srv.RestProxy.login;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Captcha extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private static final String NOSCRIPT="http://www.google.com/recaptcha/api/noscript?k=";
	private static final String CAPTCHA="http://www.google.com/recaptcha/api/";
	public static final String RES="recaptcha_response_field", CHA="recaptcha_challenge_field";

	private String key;
	private String challenge;
	private JLabel label;
	
	private boolean showed;
	
	public Captcha(String key){
		super("Captcha");
		this.key=key;
		label = new JLabel();		
		label.addMouseListener(new MouseAdapter(){
		    @Override
		    public void mouseClicked(MouseEvent e){
		        if(e.getClickCount() % 2 == 0){
		           getCaptcha();
		        }
		    }
		});
		getContentPane().add(label, BorderLayout.CENTER);
		getCaptcha();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	
	private void showImage(String url){
		Image image = null;
		try {
		    URL img = new URL(url);
		    image = ImageIO.read(img);
		} catch (IOException e) {
		}
		label.setIcon(new ImageIcon(image));
		setResizable(false);
		pack();
		if(!showed){
			setLocationRelativeTo(null);
			showed=true;
		}
		setVisible(true);
	}
	
	private void getCaptcha(){
		try{
			Document doc = Jsoup.connect(NOSCRIPT+key)
						   .get();
			String image = CAPTCHA+doc.select("img").first().attr("src");
			challenge = doc.select("#"+CHA).first().attr("value");
			showImage(image);
		}catch(Exception e){}
	}
	
	public String getChallenge(){
		return challenge;
	}
	
	public void close(){
		setVisible(false);
		dispose();
	}
}
