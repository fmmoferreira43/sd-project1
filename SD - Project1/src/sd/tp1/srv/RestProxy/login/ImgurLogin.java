package sd.tp1.srv.RestProxy.login;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.parser.ParseException;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.github.scribejava.apis.ImgurApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

public class ImgurLogin {
	private static final File COOKIE = new File(".cookie");
	private static final File TOKEN = new File(".token");
	private static final String USER_AGENT = "Imgur Proxy";
	private static final String API_KEY = "0f6d08ea6851131";
	private static final String API_SECRET = "e0f723612ddbea61cda8c70a5002cccfd700f569";
	public static final String IMGUR_CAPTCHA_KEY="6LeZbt4SAAAAAKEsafT3QzEFp5vJ1-Z23uy5mPDz";
	public static final String RES="recaptcha_response_field", CHA="recaptcha_challenge_field";
	
	private static final String INFO_RESUMING_SESSION = "Resuming previous session...";
	private static final String ERROR_RESUMING_SESSION = "Could not resume session, starting a new one...";
	private static final String INFO_AUTO_LOGIN = "Atempting auto login...";
	private static final String PROMPT_USERNAME = "Username: ";
	private static final String PROMPT_PASSWORD = "Password: ";
	private static final String PROMPT_CAPTCHA = " Captcha: ";
	private static final String OK_LOGIN = "Login successful";
	private static final String OK_RESUMING_SESSION = "Session resumed";
	private static final String ERROR_CONNECTION = "Connection error";
	private static final String ERROR_SAVING_TOKEN = "Access token could not be saved";
	private static final String ERROR_SAVING_COOKIE = "Cookie could not be saved";
	
	private OAuth20Service service;
	private OAuth2AccessToken accessToken;
	
	public ImgurLogin() throws ParseException{
		Scanner console = new Scanner(System.in);
		service = new ServiceBuilder().apiKey(API_KEY).apiSecret(API_SECRET).build(ImgurApi.instance());
		boolean hasCaptcha = false;
		
		System.out.println(INFO_RESUMING_SESSION);
		accessToken = readToken();
		if(!testSession(service, accessToken)) {
			System.err.println(ERROR_RESUMING_SESSION);
			final String authorizationUrl = service.getAuthorizationUrl();
			Map<String, String> cookies = readCookies();
			System.out.println(INFO_AUTO_LOGIN);
			while(!isLoggedin(cookies)){
				if(!hasCaptcha) {
					System.out.print(PROMPT_USERNAME);
					String user = console.nextLine();
					System.out.print(PROMPT_PASSWORD);
					String pass = console.nextLine();
					
					try { cookies = login(user, pass); }
					catch (HasCaptchaException e) { System.out.println(e.getMessage()); hasCaptcha = true; }
					catch (IncorrectLoginException e) { System.out.println(e.getMessage()); }
				} else {
					Captcha captcha = new Captcha(IMGUR_CAPTCHA_KEY);
					System.out.print(PROMPT_USERNAME);
					String user = console.nextLine();
					System.out.print(PROMPT_PASSWORD);
					String pass = console.nextLine();
					System.out.print(PROMPT_CAPTCHA);
					String response = console.nextLine();
					
					String challenge = captcha.getChallenge();
					captcha.close();
					
					try { cookies = login(user, pass, challenge, response); }
					catch (WrongCaptchaException | IncorrectLoginException e) {
						System.out.println(e.getMessage());
					}
				}
			}
			System.out.println(OK_LOGIN);
			accessToken = service.getAccessToken(getPin(authorizationUrl, cookies));
			writeToken(accessToken);
			writeCookies(cookies);
		} else {
			System.out.println(OK_RESUMING_SESSION);
		}
	}
	
	public OAuth2AccessToken getAccessToken() { return accessToken; }
	
	public OAuth20Service getService() { return service; }
	
	private static Map<String, String> login(String user, String pass, String challenge, String response) throws WrongCaptchaException, IncorrectLoginException{
		Map<String, String> cookies = null;
		try {
			Response res = Jsoup.connect("https://imgur.com/signin?minimal")
					.data("username", user)
					.data("password", pass)
					.data(CHA, challenge)
					.data(RES, response)
					.data("submit", "")
					.userAgent(USER_AGENT)
					.method(Method.POST)
					.execute();
			cookies = res.cookies();
			
			Document doc = res.parse();
			Element error = doc.select("p.error").first();
			if(error != null) {
				String desc = error.html();
				if(desc.contains("security"))
					throw new WrongCaptchaException(desc);
				else if(desc.contains("incorrect"))
					throw new IncorrectLoginException(desc);
			}
			
		} catch (IOException e) {
			System.err.println(ERROR_CONNECTION);
		}
		return cookies;
	}

	private Map<String, String> login(String user, String pass) throws HasCaptchaException, IncorrectLoginException{
		Map<String, String> cookies = null;
		try {
			Response res = Jsoup.connect("https://imgur.com/signin?minimal")
					.data("username", user)
					.data("password", pass)
					.userAgent(USER_AGENT)
					.method(Method.POST)
					.execute();
			cookies = res.cookies();
			
			Document doc = res.parse();
			Element error = doc.select("p.error").first();
			if(error != null) {
				String desc = error.html();
				if(desc.contains("captcha"))
					throw new HasCaptchaException(desc);
				else if(error.html().contains("incorrect"))
					throw new IncorrectLoginException(desc);
			}
			
		} catch (IOException e) {
			System.err.println(ERROR_CONNECTION);
		}
		return cookies;
	}
	
	private String getPin(String url, Map<String, String> cookies){
		String pin = "";
		try {
			Response res = Jsoup.connect(url)
					.cookies(cookies)
					.userAgent(USER_AGENT)
					.method(Method.GET)
					.execute();
			Document doc = res.parse();
			
			Element elAllow = doc.select("#allow").first();
			if(elAllow==null){
				return res.url().toString().split("pin=")[1];
			}
			String allow = elAllow.attr("value");
			cookies.putAll(res.cookies());
			
			Response res2 = Jsoup.connect(url)
					.cookies(cookies)
					.userAgent(USER_AGENT)
					.data("allow", allow)
					.method(Method.POST)
					.followRedirects(false)
					.execute();
			pin = res2.header("Location").split("pin=")[1];
		} catch (IOException e) {
			System.err.println(ERROR_CONNECTION);
		}
		return pin;
	}
	
	private boolean testSession(OAuth20Service service, OAuth2AccessToken token) {
		if(token == null)
			return false;
		
		OAuthRequest req = new OAuthRequest(Verb.GET, "https://api.imgur.com/3/account/me/albums/count", service);
		service.signRequest(token, req);
		return req.send().isSuccessful();
	}
	
	private boolean isLoggedin(Map<String, String> cookies){
		if(cookies == null)
			return false;
		try {
			Document doc = Jsoup.connect("https://imgur.com/")
					.cookies(cookies)
					.userAgent(USER_AGENT)
					.get();
			return doc.select(".account").first() != null;
		} catch (IOException e) {
			System.err.println(ERROR_CONNECTION);
		}
		return false;
	}
	
	private void writeToken(OAuth2AccessToken token) {
		try {
			FileOutputStream fout = new FileOutputStream(TOKEN);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(token);
			oos.close();
		}catch(IOException e) {
			System.err.println(ERROR_SAVING_TOKEN);
		}
	}
	
	private OAuth2AccessToken readToken() {
		OAuth2AccessToken token = null;
		try{
			FileInputStream fin = new FileInputStream(TOKEN);
			ObjectInputStream ois = new ObjectInputStream(fin);
			token = (OAuth2AccessToken) ois.readObject();
			ois.close();
		}catch(IOException | ClassNotFoundException e) {}
		return token;
	}

	private void writeCookies(Map<String, String> cookies){
		try {
			FileOutputStream f = new FileOutputStream(COOKIE);
			ObjectOutputStream s = new ObjectOutputStream(f);          
			s.writeObject(cookies);
			s.close();
		} catch (IOException e) {
			System.err.println(ERROR_SAVING_COOKIE);
		}  
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, String> readCookies(){
		Map<String, String> ret = null;
		try {
			FileInputStream f = new FileInputStream(COOKIE);
			ObjectInputStream s = new ObjectInputStream(f);          
			ret = (Map<String, String>) s.readObject();
			s.close();
		} catch (Exception e) {}
		return ret;
	}
}
