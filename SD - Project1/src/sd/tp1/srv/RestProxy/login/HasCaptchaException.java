package sd.tp1.srv.RestProxy.login;

public class HasCaptchaException extends Exception{
	private static final long serialVersionUID = 1L;

	public HasCaptchaException(String message) {
		super(message);
	}

}
