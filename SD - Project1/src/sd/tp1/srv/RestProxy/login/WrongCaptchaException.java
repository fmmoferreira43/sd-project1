package sd.tp1.srv.RestProxy.login;

public class WrongCaptchaException extends Exception{
	private static final long serialVersionUID = 1L;

	public WrongCaptchaException(String message) {
		super(message);
	}

}
