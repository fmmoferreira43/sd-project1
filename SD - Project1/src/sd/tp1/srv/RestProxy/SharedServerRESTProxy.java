package sd.tp1.srv.RestProxy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.json.simple.parser.ParseException;

import sd.tp1.srv.Replication.ReplicationServiceProxy;
import sd.tp1.srv.RestProxy.login.ImgurLogin;

public class SharedServerRESTProxy {
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int BUFFER_SIZE = 65536;
	static int port;	
		
	public static void main(String[] args) throws IOException, ParseException, KeyManagementException, NoSuchAlgorithmException {
		String path = args.length > 0 ? args[0] : ".";
		if(path == "help") {
			System.out.println("Use: java SharedServer [basePath]");
			System.exit(0);
		}
		
		port = getFreePort();
		URI baseUri = UriBuilder.fromUri("http://0.0.0.0/").port(port).build();
		ResourceConfig resources = new ResourceConfig();
		resources.register(GalleryBoardResource.class);
		JdkHttpServerFactory.createHttpServer(baseUri, resources);
		
		
		System.out.println("Trying to login...");
		try {
		
		//lets login
		ImgurLogin l = new ImgurLogin();
		GalleryBoardResource.service = l.getService();
		GalleryBoardResource.accessToken = l.getAccessToken();
		
		} catch (Exception ex) {
			for(StackTraceElement t : ex.getStackTrace())
				System.out.println(t);
			
			System.exit(0);
				
		}
		
		System.err.println("REST Server ready... ");
		
		//Starting replication
		ReplicationServiceProxy rep = new ReplicationServiceProxy(GalleryBoardResource.user, GalleryBoardResource.service, GalleryBoardResource.accessToken);
		rep.start();
		
		echo();
		

	}
	
	/**
	 * This function waits for a multicast discovery request, and replies...
	 * @throws IOException
	 */
	private static void echo() throws IOException {
		
		System.out.println("Starting discovery");
		
		//cria um Socket Multicast e adiciona-o a um grupo
		InetAddress group = InetAddress.getByName(MULTICAST_GROUP);
		MulticastSocket cast = new MulticastSocket(MULTICAST_PORT); 
		cast.joinGroup(group);
		byte[] data = ("REST " + port).getBytes();
		//agora esperamos por um pedido...
		while(true) {
			byte[] buffer = new byte[BUFFER_SIZE] ;			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			cast.receive(packet);
			
			buffer = packet.getData();
			String r = new String(buffer, 0, packet.getLength());
			if(r.equals("Looking")) {
				System.err.println("Replying REST " + port);	
				InetAddress source = packet.getAddress(); 						  						//obter o endereço
				DatagramPacket reply = new DatagramPacket(data, data.length, source, packet.getPort()); //criar pacote
				cast.send(reply);
			}
		}
		
	}
	
	private static int getFreePort() throws IOException {
		ServerSocket socket = new ServerSocket(0);
		return socket.getLocalPort();		
	}







}
