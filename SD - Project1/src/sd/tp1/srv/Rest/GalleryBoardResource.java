package sd.tp1.srv.Rest;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import sd.tp1.srv.Messaging.ServerMessaging;

@Path("/Gallery")
public class GalleryBoardResource {
	
	static File directory = new File(".", "Gallery");
	ServerMessaging svmsg = new ServerMessaging();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlbums() {
		
		if(!directory.exists())
			directory.mkdir();
		
		System.out.println("Fetching albums...");
		List<String> list = new ArrayList<String>();
		for(File f : directory.listFiles())
			if(f.isDirectory()) 
				list.add(f.getName());
		
		if(list.isEmpty()) {
			System.out.println("None found...");
			return Response.status(Status.NOT_FOUND).build();
		} else { 
			System.out.println("Found " + list.size() + " albuns");
			return Response.ok(list).build();
		}
	}

	/**
	 * Creates a new album if one with said name doesn't exist
	 * @param name Name of the album to create
	 * @return Status.OK if successful, Status.PRECONDITION_FAILED if the album already exists
	 * @custom.pre the album doesn't exist 
	 */
	@POST
	@Path("/CreateAlbum")
	public Response createAlbum(String name)  {
		System.out.println("Creating album...");
		if(!directory.exists())
			directory.mkdir();
		
		File f = new File(directory.toPath().toString(), name);
		if(f.exists()) {
			System.out.println("Album " + name + " already exists");
			return Response.status(Status.PRECONDITION_FAILED).build();
		} else {
			f.mkdir();
			//svmsg.addUpdatedAlbum(name);
			System.out.println("Album " + name + " created");
			return Response.ok().build();
		}
		
	}

	/**
	 * Removes an album with said name, if it exists
	 * @param name name of the album to remove
	 * @return OK if sucessful, NOT_FOUND if the album doesnt exist
	 * @custom.pre album exists
	 */
	@DELETE
	@Path("/DeleteAlbum/{name}")
	public Response deleteAlbum(@PathParam("name") String name) {
		
		if(!directory.exists())
			directory.mkdir();
		
		File f = new File(directory.toPath().toString(),name);
		if(!f.exists()){
			System.out.println("Album " + name + " doesn't exist.");
			return Response.status(Status.NOT_FOUND).build();
		}else{
			f.delete();
			//svmsg.addUpdatedAlbum(name);
			System.out.println("Album " + name + " deleted.");
			return Response.ok().build();
		}
	}

	/**
	 * Returns a list of picture names
	 * @param album album where the pictures are located
	 * @return OK with list or NOT_FOUND if the album doesnt exist or the album is empty
	 */
	@GET
	@Path("/ListPictures/{album}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListOfPictures(@PathParam("album") String album) {
		
		if(!directory.exists())
			directory.mkdir();
		
		File d = new File(directory.toPath().toString(), album);
		if(d.exists()) {
			List<String> list = new ArrayList<String>();
			for(File f : d.listFiles()) {
				if(!f.isDirectory()) {
					list.add(f.getName());
				}
			}
			
			if(list.isEmpty())
				return Response.status(Status.NOT_FOUND).build();
			else
				//svmsg.addUpdatedAlbum(album);
				return Response.ok(list).build();
			
		} else return Response.status(Status.NOT_FOUND).build(); 
		
	}

	/**
	 * Returns a data of a picture.
	 * @param album
	 * @param picture
	 * @return OK with data or NOT_FOUND if album or picture doesnt exist.
	 * @throws IOException
	 */
	@GET
	@Path("/get/{album}/{picture}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPictureData(@PathParam("album") String album, @PathParam("picture") String picture) throws IOException {
		
		if(!directory.exists())
			directory.mkdir();
		
		File d = new File(directory.toPath().toString(), album);
		if(d.exists()){
			File p = new File(d.toPath().toString(), picture);
			if(!p.exists()){
				System.out.println("Picture " + picture + " doesn't exist.");
				return Response.status(Status.NOT_FOUND).build();
			}else{
				System.out.println("Picture " + picture + " readind all bytes.");
				return Response.ok(Files.readAllBytes(p.toPath())).build();
			}
			
		}else{
			System.out.println("Album " + album + " doesn't exist.");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	
	/**
	 * uploads a picture
	 * @param album
	 * @param name
	 * @param data
	 * @return OK uploads the picture or NOT_FOUND if album or picture doesnt exist.
	 */
	@POST
	@Path("/Upload/{album}/{name}")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response uploadPicture(@PathParam("album") String album, @PathParam("name") String name, byte[] data){
		
		System.out.println("Recieved picture");
		if(!directory.exists())
			directory.mkdir();
		
		File d = new File(directory.toPath().toString(), album);
		if(d.exists()){
			File p = new File(d.getPath().toString(), name);
			if(p.exists()){
				System.out.println("Picture " + name + " already exist.");
				return Response.status(Status.NOT_FOUND).build();
			}else{
				try (OutputStream out = new BufferedOutputStream(
						Files.newOutputStream(p.toPath(), CREATE, APPEND))) {
						out.write(data, 0, data.length);
					} catch (IOException x) {
					    	System.err.println(x);
					}
				//svmsg.addUpdatedAlbum(album);
				System.out.println("Picture " + name + " uploaded.");
				return Response.ok().build();
			}
		}else{
			System.out.println("Album " + album + " doesn't exist.");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Deletes a picture.
	 * @param album
	 * @param picture
	 * @return OK if successful or NOT_FOUND if album or picture doesnt exist.
	 */
	@DELETE
	@Path("/DeletePicture/{album}/{picture}")
	public Response deletePicture(@PathParam("album")String album, @PathParam("picture")String picture){
		if(!directory.exists())
			directory.mkdir();
		
		File d = new File(directory.getPath().toString(), album);
		if(d.exists()){
			File p = new File(d.getPath().toString(), picture);
			if(!p.exists()){
				System.out.println("Picture " + picture + " doesn't exist.");
				return Response.status(Status.NOT_FOUND).build();
			}else{
				p.delete();
				//svmsg.addUpdatedAlbum(album);
				System.out.println("Picture " + picture + " deleted.");
				return Response.ok().build();
			}
		}else{
			System.out.println("Album " + album + " doesn't exist.");
			return Response.status(Status.NOT_FOUND).build();
		}
		
	}

	
	/**
	 * return the space of a directory.
	 * @return OK if sucessfull or NOT_FOUND if directory is Empty
	 */
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response directorySize(){
		if(!directory.exists()){
			return Response.status(Status.NOT_FOUND).build();
		}else{
			return Response.ok(directory.getTotalSpace()).build();
		}
	}
	
	
	public static final String PERMISSION_TYPE = "Access-Control-Allow-Origin";
	public static final String PERMISSION = "*";
	
	@GET
	@Path("/search/{pattern}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPaternList(@PathParam("pattern") String pattern) {
		if(!directory.exists())
			directory.mkdir();
		
		
		List<String> list = new ArrayList<String>();
		for(File d : directory.listFiles()) {
			for(String f : d.list()) {
				String name =  d.getName() + "/" + f;
				if(name.contains(pattern))
					list.add("http://localhost:8080/Gallery/get/" + name);
			}
		}
		
		if(list.isEmpty())
			Response.status(Status.NOT_FOUND).build();
		
		Response rep = Response.ok(list).build();
		rep.getHeaders().add(PERMISSION_TYPE, PERMISSION);
		return rep;
		
	}
	
	
	@GET
	@Path("/lastModified/{album}/{picture}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPictureLastChange(@PathParam("album")String album, @PathParam("picture")String picture) {
		if(!directory.exists())
			directory.mkdir();
		
		File d = new File(directory.toPath().toString(), album);
		if(d.exists()){
			File p = new File(d.toPath().toString(), picture);
			if(!p.exists()){
				return Response.status(Status.NOT_FOUND).build();
			}else{
				long timestamp = p.lastModified();
				return Response.ok(timestamp).build();
			}
			
		}else{
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	


}
