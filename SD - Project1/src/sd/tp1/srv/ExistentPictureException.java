package sd.tp1.srv;

public class ExistentPictureException extends Exception {
	public ExistentPictureException(String message) {
		super(message);
	}
}
