package sd.tp1.srv;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Server{
	
	public static final String URL = "http://%s:%s/Gallery";
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int BUFFER_SIZE = 65536;
	
	/**
	 * Returns the listing of albums from this server
	 * @return String array with the albums names
	 */
	@WebMethod
	String[] getAlbums();
	
	/**
	 * Creates an album
	 * @param name new name of the album
	 * @throws ExistingAlbumException if there's already an album
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	void createAlbum(String name) throws ExistingAlbumException, IOException;
	
	/**
	 * Deletes an album
	 * @param name name of the album to be deleted
	 * @throws InexistentAlbumException if there's not such album
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	void deleteAlbum(String name) throws InexistentAlbumException, IOException;
	
	
	/**
	 * Lists the pictures from a certain album
	 * @param album name of the album to fetch the list from
	 * @throws InexistentAlbumException if the album does not exist
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	String[] getListOfPictures(String album) throws InexistentAlbumException, IOException;
	
	/**
	 * Fetches the data from a certain picture
	 * @param album the name of the album where the picture is inserted
	 * @param picture the name of the picture
	 * @return a byte array with the picture data
	 * @throws InexistentAlbumException if the album doesn't exist
	 * @throws InexistentPictureException if the picture doesn't exist
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	byte[] getPictureData(String album, String picture) throws InexistentAlbumException, InexistentPictureException, IOException;
	
	/**
	 * Uploads a new picture to the server
	 * @param album The name of the destination album
	 * @param picture The name of the picture
	 * @param data	The picture data to upload
	 * @throws InexistentAlbumException If the album doesn't exist
	 * @throws ExistentPictureException If the Picture already exists
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	void uploadPicture(String album, String picture, byte[] data) throws InexistentAlbumException, ExistentPictureException, IOException;
	
	/**
	 * Deletes a certain picture from an album
	 * @param album The name of the containing album
	 * @param picture The name of the picture
	 * @throws InexistentAlbumException If the album doesn't exist
	 * @throws ExistentPictureException  If the picture exists
	 * @throws IOException general filesystem error
	 */
	@WebMethod
	void deletePicture(String album, String picture) throws InexistentAlbumException, IOException, InexistentPictureException;
		
	/**
	 * Checks the size of the current directory
	 * @return returns the size of the current directory
	 * @throws NotFileServerException if the server is not a File Server
	 */
	@WebMethod
	long directorySize() throws NotFileServerException;
	
	/**
	 * Returns the last change of a certain picture...
	 * @param album
	 * @param picture
	 * @return
	 * @throws InexistentAlbumException
	 * @throws InexistentPictureException
	 * @throws IOException
	 */
	@WebMethod
	long lastChange(String album, String picture) throws InexistentAlbumException, InexistentPictureException, IOException;

	
}
