package sd.tp1.srv;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import sd.tp1.srv.Replication.ReplicationService;

/**
 * Our file server
 * @author João Borrego, Francisco Ferreira
 *
 */
@WebService
public class SharedServer implements Server {
	
	private File directory;

	
	public SharedServer() {
		this(".");
	}
	
	protected SharedServer(String root) {
		super();
		this.directory = new File(root, "Gallery");
		if(!this.directory.exists())
			this.directory.mkdir();
	}
	
	
	public static void main(String args[]) throws Exception {
		String path = args.length > 0 ? args[0] : ".";
		if(path == "help") {
			System.out.println("Use: java SharedServer [basePath]");
			System.exit(0);
		}
		
		int port = getFreePort();
		
		Endpoint.publish(String.format(URL, "0.0.0.0", port), new SharedServer(path));
		System.err.println("Content Provider started");
		
		//start replication service
		ReplicationService rep = new ReplicationService("Gallery");
		rep.start();
		
		//cria um Socket Multicast e adiciona-o a um grupo
		InetAddress group = InetAddress.getByName(MULTICAST_GROUP);
		MulticastSocket cast = new MulticastSocket(MULTICAST_PORT); 
		cast.joinGroup(group);
		
		byte[] data = ("WSDL " + port).getBytes();
		//agora esperamos por um pedido...
		while(true) {
			byte[] buffer = new byte[BUFFER_SIZE] ;			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			cast.receive(packet);
			
			buffer = packet.getData();
			String r = new String(buffer, 0, packet.getLength());
			System.err.println("Recieved packet " + r + " from " + packet.getAddress() + ":" + packet.getPort());
			
			if(r.equals("Looking")) {
				System.err.println("Replying WSDL " + port);				
				InetAddress source = packet.getAddress(); 						  						//obter o endereço
				DatagramPacket reply = new DatagramPacket(data, data.length, source, packet.getPort()); //criar pacote
				cast.send(reply);
			}
		}
		
	}

	@Override
	public void createAlbum(String name) throws ExistingAlbumException, IOException {
				
		File f = new File(directory.toPath().toString(), name);
		if(!f.exists()){
			f.mkdir();
			//producer.send(new ProducerRecord<String, String>("added-album", name, ""));
		}else throw new ExistingAlbumException();
		
	}
	
	@WebMethod
	public String[] getAlbums() {
		List<String> l = new ArrayList<String>();
		for(File f : directory.listFiles()) {
			if(f.isDirectory())
				l.add(f.getName());
			
		}
		
		String[] arr = new String[l.size()];
		l.toArray(arr);
		return arr;
	}

	@Override
	public void deleteAlbum(String name) throws InexistentAlbumException, IOException {
		
		File f = new File(directory.toPath().toString(), name);
		if(f.exists()) {
			deleteFolder(f);
		} throw new InexistentAlbumException();
	}
	
	/**
	 * Recursivelly deletes the folder and all its subfolders and content
	 * @param folder folder to delete
	 */
	private void deleteFolder(File folder) {
		for(File f : folder.listFiles()) {
			if(f.isDirectory())
				deleteFolder(f);
			else
				f.delete();
		}
		folder.delete();
	}

	@Override
	public String[] getListOfPictures(String album) throws InexistentAlbumException, IOException {
		
		File f = new File(directory.toPath().toString(), album);
		if(f.exists()){
			List<String> l = new ArrayList<String>();
			for(File f1 : f.listFiles())
				if(!f1.isDirectory())
					l.add(f1.getName());
			String[] arr = new String[l.size()];
			l.toArray(arr);
			return arr;
		} throw new InexistentAlbumException();
	}

	@Override
	public byte[] getPictureData(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		// TODO Auto-generated method stub
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if(albumOnDirectory.exists()){
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if(pictureOnAlbum.exists()){
				byte[] b = Files.readAllBytes(pictureOnAlbum.toPath());
				//System.out.println(picture + " file size: " + b.length);
				return b;
			} else throw new InexistentPictureException();
		}else throw new InexistentAlbumException();
	}

	@Override
	public void uploadPicture(String album, String picture, byte[] data)
			throws InexistentAlbumException, ExistentPictureException, IOException {
		// TODO Auto-generated method stub
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if(albumOnDirectory.exists()){
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if(!pictureOnAlbum.exists()){
				try (OutputStream out = new BufferedOutputStream(
					Files.newOutputStream(pictureOnAlbum.toPath(), CREATE, APPEND))) {
					out.write(data, 0, data.length);
				} catch (IOException x) {
				    	System.err.println(x);
				}
			}else throw new ExistentPictureException("Picture already exists");
		}else throw new InexistentAlbumException();
	}

	@Override
	public void deletePicture(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		// TODO Auto-generated method stub
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if(albumOnDirectory.exists()){
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if(pictureOnAlbum.exists()){
				pictureOnAlbum.delete();
			}else throw new InexistentPictureException();
		}else throw new InexistentAlbumException();
	}

	@Override
	public long directorySize() throws NotFileServerException {
		if(directory.exists()){
			return directory.getTotalSpace();
		}else throw new NotFileServerException();
	}

	@Override
	public long lastChange(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if(albumOnDirectory.exists()){
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if(pictureOnAlbum.exists()) {
				return pictureOnAlbum.lastModified();
			}else throw new InexistentPictureException();
		}else throw new InexistentAlbumException();
		
	}
	
	
	private static int getFreePort() throws IOException {
		ServerSocket socket = new ServerSocket(0);
		return socket.getLocalPort();		
	}
	
	

}
