package sd.tp1.srv;

public class InexistentAlbumException extends Exception {
	
	public InexistentAlbumException(String message) {
		super(message);
	}

	public InexistentAlbumException() {
		super("Album does not exist...");
	}

}
