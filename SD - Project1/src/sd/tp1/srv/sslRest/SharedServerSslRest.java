package sd.tp1.srv.sslRest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.URI;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Properties;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.core.UriBuilder;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

import sd.tp1.srv.RestProxy.GalleryBoardResource;

public class SharedServerSslRest {

	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int BUFFER_SIZE = 65536;
	public static final File KEYSTORE = new File("./server.jks");
	public static final char[] JKS_PASSWORD = "changeit".toCharArray();
	public static final char[] KEY_PASSWORD = "changeit".toCharArray();
		
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException, UnrecoverableKeyException {
		String path = args.length > 0 ? args[0] : ".";
		if(path == "help") {
			System.out.println("Use: java SharedServer [basePath]");
			System.exit(0);
		}
		
		
		int port = getFreePort();
		URI baseUri = UriBuilder.fromUri("https://0.0.0.0/").port(port).build();
		ResourceConfig resources = new ResourceConfig();
		resources.register(GalleryBoardResource.class);
		//HttpServer server = JdkHttpServerFactory.createHttpServer(baseUri, resources);

		
		
		SSLContext sslContext = SSLContext.getInstance("TLSv1");
		KeyStore ks = KeyStore.getInstance("JKS");
		try(InputStream is = new FileInputStream(KEYSTORE)){
			ks.load(is, JKS_PASSWORD);
		}
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, JKS_PASSWORD);
		
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(ks);
		HttpServer server = JdkHttpServerFactory.createHttpServer(baseUri, resources, sslContext);
		
		
		System.out.println("REST SSL Server ready... ");
		
		//cria um Socket Multicast e adiciona-o a um grupo
		InetAddress group = InetAddress.getByName(MULTICAST_GROUP);
		MulticastSocket cast = new MulticastSocket(MULTICAST_PORT); 
		cast.joinGroup(group);
		byte[] data = ("REST SSL " + port).getBytes();
		
		System.out.println("Starting Discovery");
		//agora esperamos por um pedido...
		while(true) {
			byte[] buffer = new byte[BUFFER_SIZE] ;			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			cast.receive(packet);
			
			buffer = packet.getData();
			String r = new String(buffer, 0, packet.getLength());
			System.err.println("Recieved packet " + r + " from " + packet.getAddress() + ":" + packet.getPort());
			if(r.equals("Looking")) {
				System.err.println("Replying REST SSL " + port);				
				InetAddress source = packet.getAddress(); 						  						//obter o endereço
				DatagramPacket reply = new DatagramPacket(data, data.length, source, packet.getPort()); //criar pacote
				cast.send(reply);
			}
		}

	}
	

	private static int getFreePort() throws IOException {
		ServerSocket socket = new ServerSocket(0);
		return socket.getLocalPort();		
	}

}
