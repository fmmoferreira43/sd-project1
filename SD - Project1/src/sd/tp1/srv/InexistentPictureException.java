package sd.tp1.srv;

public class InexistentPictureException extends Exception {
	
	public InexistentPictureException(String message) {
		super(message);
	}
	
	public InexistentPictureException() {
		super("Picture does not exist...");
	}

}
