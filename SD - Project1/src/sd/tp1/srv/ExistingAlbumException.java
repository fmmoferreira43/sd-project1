package sd.tp1.srv;

public class ExistingAlbumException extends Exception {
	
	public ExistingAlbumException(String message) {
		super(message);
	}
	
	public ExistingAlbumException() {
		super("Album already exists...");
	}

}
