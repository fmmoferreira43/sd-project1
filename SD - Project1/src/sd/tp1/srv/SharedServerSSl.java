package sd.tp1.srv;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.ws.Endpoint;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsServer;

import sd.tp1.srv.Replication.ReplicationService;

/**
 * Our file server
 * 
 * @author João Borrego, Francisco Ferreira
 *
 */
@WebService
public class SharedServerSSl implements Server {

	static final File KEYSTORE = new File("./server.jks");
	static final char[] JKS_PASSWORD = "changeit".toCharArray();
	static final char[] KEY_PASSWORD = "changeit".toCharArray();

	private File directory;
	

	public SharedServerSSl() {
		this(".");
	}

	protected SharedServerSSl(String root) {
		super();
		this.directory = new File(root, "Gallery");
		if (!this.directory.exists())
			this.directory.mkdir();
		
		
		
	}

	public static void main(String args[]) throws Exception {
		final int Port = getFreePort();
		String url =  "https://0.0.0.0:" + Port + "/Gallery";

		System.out.println("Going to use port " + Port);
		
		// Load and initialize the server's java keystore.
		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		KeyStore store = KeyStore.getInstance("JKS");
		try (FileInputStream fis = new FileInputStream(KEYSTORE)) {
			store.load(fis, JKS_PASSWORD);
			keyFactory.init(store, KEY_PASSWORD);
		}

		// Prepare the server's trust manager
		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustFactory.init(store);

		// Create and initialize the ssl context.
		SSLContext ssl = SSLContext.getInstance("TLS");
		ssl.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), new SecureRandom());

		// Create the HTTPS server using the ssl context.
		HttpsConfigurator configurator = new HttpsConfigurator(ssl);
		HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress("0.0.0.0", Port), -1);
		httpsServer.setHttpsConfigurator(configurator);
		HttpContext httpContext = httpsServer.createContext("/Gallery");
		httpsServer.start();

		// Instantiate the soap webservice and publish it on the the https
		// server.
		SharedServerSSl impl = new SharedServerSSl(".");
		Endpoint ep = Endpoint.create(impl);
		ep.publish(httpContext);

		// publish the same soap web service instance on a different port using
		// insecure http protocol
		// This is used to generate the stubs with wsimport
		// Once the stubs are generated, this line should be removed so that
		// the web service is not also available over insecure channels.
		//String url2 = url.replace("https", "http").replace("" + Port, "" +
		//(Port+1));
		//Endpoint.publish(url2, impl);

		

		System.out.println("Server Started.");
		
		
		//start replication service
		ReplicationService rep = new ReplicationService("Gallery");
		rep.start();

		// cria um Socket Multicast e adiciona-o a um grupo
		InetAddress group = InetAddress.getByName(MULTICAST_GROUP);
		MulticastSocket cast = new MulticastSocket(MULTICAST_PORT);
		cast.joinGroup(group);

		byte[] data = ("WSDL SSL " + Port).getBytes();
		// agora esperamos por um pedido...
		while (true) {
			byte[] buffer = new byte[BUFFER_SIZE];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			cast.receive(packet);

			buffer = packet.getData();
			String r = new String(buffer, 0, packet.getLength());
			System.err.println("Recieved packet " + r + " from " + packet.getAddress() + ":" + packet.getPort());

			if (r.equals("Looking")) {
				System.err.println("Replying WSDL SSL " + Port);
				InetAddress source = packet.getAddress(); // obter o endereço
				DatagramPacket reply = new DatagramPacket(data, data.length, source, packet.getPort()); // criar
																										// pacote
				cast.send(reply);
			}
		}

	}

	@Override
	public void createAlbum(String name) throws ExistingAlbumException, IOException {

		File f = new File(directory.toPath().toString(), name);
		if (!f.exists()){
			f.mkdir();
		}
		else
			throw new ExistingAlbumException();

	}

	@WebMethod
	public String[] getAlbums() {

		List<String> l = new ArrayList<String>();
		for (File f : directory.listFiles()) {
			if (f.isDirectory())
				l.add(f.getName());

		}

		String[] arr = new String[l.size()];
		l.toArray(arr);
		return arr;
	}

	@Override
	public void deleteAlbum(String name) throws InexistentAlbumException, IOException {

		File f = new File(directory.toPath().toString(), name);
		if (f.exists()) {
			deleteFolder(f);
		}
		throw new InexistentAlbumException();
	}

	/**
	 * Recursivelly deletes the folder and all its subfolders and content
	 * 
	 * @param folder
	 *            folder to delete
	 */
	private void deleteFolder(File folder) {
		for (File f : folder.listFiles()) {
			if (f.isDirectory())
				deleteFolder(f);
			else
				f.delete();
		}
		folder.delete();
	}

	@Override
	public String[] getListOfPictures(String album) throws InexistentAlbumException, IOException {

		File f = new File(directory.toPath().toString(), album);
		if (f.exists()) {
			List<String> l = new ArrayList<String>();
			for (File f1 : f.listFiles())
				if (!f1.isDirectory())
					l.add(f1.getName());
			String[] arr = new String[l.size()];
			l.toArray(arr);
			return arr;
		}
		throw new InexistentAlbumException();
	}

	@Override
	public byte[] getPictureData(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		// TODO Auto-generated method stub
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if (albumOnDirectory.exists()) {
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if (pictureOnAlbum.exists()) {
				byte[] b = Files.readAllBytes(pictureOnAlbum.toPath());
				// System.out.println(picture + " file size: " + b.length);
				return b;
			} else
				throw new InexistentPictureException();
		} else
			throw new InexistentAlbumException();
	}

	@Override
	public void uploadPicture(String album, String picture, byte[] data)
			throws InexistentAlbumException, ExistentPictureException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Recieving picture...");
		
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if (albumOnDirectory.exists()) {
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if (!pictureOnAlbum.exists()) {
				try {
					OutputStream out = new BufferedOutputStream(
							Files.newOutputStream(pictureOnAlbum.toPath(), CREATE, APPEND));
					out.write(data, 0, data.length);
				} catch (IOException x) {
					System.err.println(x);
				}
			} else
				throw new ExistentPictureException("Picture already exists");
		} else
			throw new InexistentAlbumException();
		
		System.out.print("Picture recieved...");
		
	}

	@Override
	public void deletePicture(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Deleting picture...");
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if (albumOnDirectory.exists()) {
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if (pictureOnAlbum.exists()) {
				pictureOnAlbum.delete();
			} else
				throw new InexistentPictureException();
		} else
			throw new InexistentAlbumException();
		
		
		System.out.println("Picture deleted...");
		
	}

	@Override
	public long directorySize() throws NotFileServerException {
		if (directory.exists()) {
			return directory.getTotalSpace();
		} else
			throw new NotFileServerException();
	}
	
	@Override
	public long lastChange(String album, String picture)
			throws InexistentAlbumException, InexistentPictureException, IOException {
		
		File albumOnDirectory = new File(directory.toPath().toString(), album);
		if(albumOnDirectory.exists()){
			File pictureOnAlbum = new File(albumOnDirectory.toPath().toString(), picture);
			if(pictureOnAlbum.exists()) {
				return pictureOnAlbum.lastModified();
			}else throw new InexistentPictureException();
		}else throw new InexistentAlbumException();
		
	}
	

	private static int getFreePort() throws IOException {
		ServerSocket socket = new ServerSocket(0);
		int s = socket.getLocalPort();
		socket.close();
		return s;		
	}
}