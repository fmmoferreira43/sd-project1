package sd.tp1.srv.Replication;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

import sd.tp1.clt.RestPlusSsl.SharedGalleryContentProviderRESTPlusSSL.InsecureHostNameVerifier;
import sd.tp1.clt.RestPlusSsl.SharedGalleryContentProviderRESTPlusSSL.InsecureTrustManager;
import sd.tp1.clt.ssl.ws.SharedServerSSl;
import sd.tp1.clt.ssl.ws.SharedServerSSlService;
import sd.tp1.clt.ws.IOException_Exception;
import sd.tp1.clt.ws.InexistentAlbumException_Exception;
import sd.tp1.clt.ws.InexistentPictureException_Exception;
import sd.tp1.clt.ws.SharedServer;
import sd.tp1.clt.ws.SharedServerService;

public class ReplicationService extends Thread {

	public static final long UPDATE_INTERVAL = 1000;
	public static final long REQUEST_INTERVAL = 2000;
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int BUFFER_SIZE = 65536;
	public static final String URL = "%s://%s:%s/Gallery";

	private File dir;
	private Map<String, Long> timeMap;
	private long lastRequest;

	
	private SSLContext sc;
	
	
	/**
	 * This default constructor is to be used by replication that doesn't provide for a file system unit
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	protected ReplicationService() throws NoSuchAlgorithmException, KeyManagementException {
		this.timeMap = new HashMap<String, Long>();
		this.lastRequest = -REQUEST_INTERVAL;

	}
	

	public ReplicationService(String dir) throws KeyManagementException, NoSuchAlgorithmException {
		this();
		this.dir = new File(".", dir);
		
	}


	public void run() {

			InetAddress group;
			MulticastSocket cast;
			byte[] data = "Looking".getBytes();
		
			try {
			
				group = InetAddress.getByName(MULTICAST_GROUP);
				cast = new MulticastSocket(MULTICAST_PORT);
				cast.joinGroup(group);
				
			
			} catch (Exception ex) {
				ex.printStackTrace();
				return;
			}

			while (true) {	
				try {
					if(lastRequest + REQUEST_INTERVAL < System.currentTimeMillis()) {
						lastRequest = System.currentTimeMillis();
						System.out.println("Replication: Listening for servers...");
						byte[] b = String.format("%s", "Looking").getBytes();
						DatagramPacket p = new DatagramPacket(b, b.length, group, MULTICAST_PORT);
						try {
							cast.send(p);
						} catch (Exception e) {
							System.out.println(e.getMessage());
							continue;
						}
						
					} 
					
	
					byte[] buffer = new byte[BUFFER_SIZE];
					DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
					cast.receive(packet);
	
					buffer = packet.getData();
					String r = new String(buffer, 0, packet.getLength());
					String[] mess = r.split(" ");
					final int port = 8080;
					//String url = String.format(URL, packet.getAddress().getHostAddress(), port);
					
					if(isItself(packet.getAddress())) {
						continue;
					}
					
					if (mess[0].contains("REST")) {
						//SLL Stuff
						
						
						String url = null;
						if(mess[1].equals("SSL")) {
							url = String.format(URL, "https", packet.getAddress().getHostAddress(), mess[2]);
						} else {
							url = String.format(URL, "http", packet.getAddress().getHostAddress(), mess[1]);
						}
						
						sc = SSLContext.getInstance("TLSv1");
						TrustManager[] trustAllCerts = { new InsecureTrustManager()};
						sc.init(null, trustAllCerts, new java.security.SecureRandom());
						if(timeMap.containsKey(url) && 
								timeMap.get(url) + UPDATE_INTERVAL < System.currentTimeMillis())
							continue;
						
						replicateFromRest(url, mess[1].equals("SSL"));
					} else if (mess[0].equals("WSDL")) {
						
						String url = null;
						if(mess[1].equals("SSL")) {
							url = String.format(URL, "https", packet.getAddress().getHostAddress(), mess[2]);
							sc = SSLContext.getInstance("TLSv1");
							TrustManager[] trustAllCerts = { new InsecureTrustManager()};
							sc.init(null, trustAllCerts, new java.security.SecureRandom());
							HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
							HttpsURLConnection.setDefaultHostnameVerifier( new InsecureHostNameVerifier());
							
							if(timeMap.containsKey(url) && 
									timeMap.get(url) + UPDATE_INTERVAL < System.currentTimeMillis())
								continue;	
							replicateFromSoapSsl(url);
						} else {
							url = String.format(URL, "http", packet.getAddress().getHostAddress(), mess[1]);
							if(timeMap.containsKey(url) && 
									timeMap.get(url) + UPDATE_INTERVAL < System.currentTimeMillis())
								continue;	
							replicateFromSoap(url);
						}
						
						
					} 
				
				} catch (Exception ex) {
					//ignore and keep looping
				}
			
			}
		

	}

	/**
	 * This replicates a soap server
	 * 
	 * @param address
	 *            server address
	 * @throws MalformedURLException
	 * @throws InexistentPictureException_Exception 
	 * @throws InexistentAlbumException_Exception 
	 * @throws IOException_Exception 
	 */
	protected void replicateFromSoap(String url) throws MalformedURLException  {
		
		SharedServerService serv;
		serv = new SharedServerService(new URL(url));
		SharedServer s = serv.getSharedServerPort();
		List<String> lst = new ArrayList<String>();
		for (String a : s.getAlbums()) {
			lst.add(a);
			try {
				for (String pic : s.getListOfPictures(a)) {
					long timestamp = s.lastChange(a, pic);
					if (!hasPicture(a, pic) || isNewVersion(a, pic, timestamp)) {
						byte[] b = s.getPictureData(a, pic);
						writeImage(a, pic, b);
					}
				}
			} catch (IOException_Exception | InexistentAlbumException_Exception
					| InexistentPictureException_Exception e) {
				continue;
			}
		}

	}

	/**
	 * This replicates a rest server
	 * 
	 * @param address
	 *            server address
	 */
	protected void replicateFromRest(String url, boolean ssl) {
		ClientConfig config = new ClientConfig();
		
		Client client = null;
		if(ssl) {
			client = ClientBuilder.newBuilder()
					.hostnameVerifier(new InsecureHostNameVerifier())
					.sslContext(sc)
					.build();
		} else {
			client = ClientBuilder.newClient(config);
		}
		WebTarget target = client.target(url);

		Response r = target.request(MediaType.APPLICATION_JSON).get();
		if (r.getStatus() != Status.OK.getStatusCode()) {
			return;
		}

		List<String> albums = r.readEntity(ArrayList.class);

		for (String album : albums) {

			Response r1 = target.path("/ListPictures/" + album).request().accept(MediaType.APPLICATION_JSON).get();
			if (r1.getStatus() != Status.OK.getStatusCode())
				continue;
			List<String> pictures = r.readEntity(ArrayList.class);
			for (String pic : pictures) {
				
				Response r3 = target.path("/lastModified/" + album + "/" + pic).request(MediaType.APPLICATION_OCTET_STREAM).get();
				if(r.getStatus() != Status.OK.getStatusCode())
					continue;
				
				long timestamp = r3.readEntity(long.class);
				if (!hasPicture(album, pic) || isNewVersion(album, pic, timestamp)) {
					Response r2 = target.path("/PictureData/" + album + "/" + pic).request().get();
					if (r.getStatus() != Status.OK.getStatusCode())
						continue;
					byte[] b = r.readEntity(byte[].class);
					writeImage(album, pic, b);

				}
			}

		}

	}
	
	
	private void replicateFromSoapSsl(String url) throws MalformedURLException {
		SharedServerSSlService serv;
		serv = new SharedServerSSlService(new URL(url));
		SharedServerSSl s = serv.getSharedServerSSlPort();
		List<String> lst = new ArrayList<String>();
		for (String a : s.getAlbums()) {
			lst.add(a);
			try {
				for (String pic : s.getListOfPictures(a)) {
					long timestamp = s.lastChange(a, pic);
					if (!hasPicture(a, pic) || isNewVersion(a, pic, timestamp)) {
						byte[] b = s.getPictureData(a, pic);
						writeImage(a, pic, b);
					}
				}
			} catch (sd.tp1.clt.ssl.ws.IOException_Exception | sd.tp1.clt.ssl.ws.InexistentAlbumException_Exception
					| sd.tp1.clt.ssl.ws.InexistentPictureException_Exception e) {
				continue;
			}
		}
	}
	
	
	/**
	 * This checks if there's a new version of a file
	 * @param album album name
	 * @param picture picture name
	 * @param lastChange last change to compare
	 * @return true if there's, false if there isn't
	 */
	private boolean isNewVersion(String album, String picture, long lastChange) {
		File a = new File(dir.toPath().toString(), album);
		File p = new File(a.toPath().toString(), picture);
		return p.lastModified() > lastChange;		
	}

	/**
	 * Checks if the picture exists in the file system
	 * @param album album name
	 * @param picture picture name
	 * @return true if it exists, false if it doesnt
	 */
	protected boolean hasPicture(String album, String picture) {

		File a = new File(dir.toPath().toString(), album);
		if(!a.exists())
			return false;
		
		File p = new File(a.toPath().toString(), picture);
		
		if (!p.exists())
			return false;
		if (p.isDirectory())
			return false;
		return true;
	}

	/**
	 * This method writes the image to the server
	 * @param album album name
	 * @param picture picture name
	 * @param data byte array with the picture data
	 */
	protected void writeImage(String album, String picture, byte[] data) {

		File a = new File(dir.toPath().toString(), album);
		if(!a.exists())
			a.mkdir();		
		File p = new File(a.toPath().toString(), picture);

		try (OutputStream out = Files.newOutputStream(p.toPath(), CREATE, APPEND)) {
			out.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	private boolean isItself(InetAddress address) throws UnknownHostException {
		return InetAddress.getLocalHost().equals(address);			
	}
	

}
