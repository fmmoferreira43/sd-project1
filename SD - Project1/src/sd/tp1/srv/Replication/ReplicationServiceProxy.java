package sd.tp1.srv.Replication;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

public class ReplicationServiceProxy extends ReplicationService {

	private String user;
	private OAuth20Service service;
	private OAuth2AccessToken accessToken;
		
	
	public ReplicationServiceProxy(String user, OAuth20Service service, OAuth2AccessToken accessToken) throws KeyManagementException, NoSuchAlgorithmException {
		super();
		this.user = user;
		this.service = service;
		this.accessToken = accessToken;
	}

	
	private String getAlbumId(String album) {
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				"https://api.imgur.com/3/account/" + user + "/albums", service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();

		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			return null;
		}
		
		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		
		String albumId = null;		
		while(albumsIt.hasNext()) {
			JSONObject obj = (JSONObject)albumsIt.next();
			String title = (String)obj.get("title");
			if(title.equals(album)) {
				return (String)obj.get("id");
			}
		}
		return null;
	}
	
	private String getPictureId(String albumId, String picture) {
		String url = String.format("https://api.imgur.com/3/album/%s/images", albumId);
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			return null;
		}
		
		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		while (albumsIt.hasNext()) {
			JSONObject elem = (JSONObject) albumsIt.next();
			String name = (String) elem.get("name");
			if(picture.equals(name))
				return (String) elem.get("id");
		}
		
		return null;
	}
	
	
	
	@Override
	protected boolean hasPicture(String album, String picture) {
		
		String albumId = getAlbumId(album);
		if(albumId == null)
			return false;
		
		String pictureId = getPictureId(albumId, picture);
		if(pictureId == null)
			return false;

		return true;
		
	}
	
	
	private String createNewAlbum(String album) {
		String url = String.format("https://api.imgur.com/3/album?title=%s", album);
		OAuthRequest albumsReq = new OAuthRequest(Verb.POST,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		String succ = (String) res.get("success");
		boolean isSuccess = Boolean.getBoolean(succ);
		
		if(isSuccess) {
			return (String)res.get("id");
		}
		
		return null;
		
	}
	

	@Override
	protected void writeImage(String album, String picture, byte[] data) {
		String albumId = getAlbumId(album);
		if(albumId == null) {
			albumId = createNewAlbum(album);
			if(albumId == null) return;
		}
		
		String url = String.format("https://api.imgur.com/3/album/%s/images", albumId);
		OAuthRequest albumsReq = new OAuthRequest(Verb.GET,
				url, service);
		service.signRequest(accessToken, albumsReq);
		final Response albumsRes = albumsReq.send();
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		try {
			res = (JSONObject) parser.parse(albumsRes.getBody());
		} catch (ParseException e) {
			return;
		}
		
		JSONArray albums = (JSONArray) res.get("data");
		Iterator albumsIt = albums.iterator();
		String pictureId = null, deleteHash = null;
		while (albumsIt.hasNext()) {
			JSONObject elem = (JSONObject) albumsIt.next();
			String name = (String) elem.get("name");
			if(picture.equals(name)) {
				pictureId = (String) elem.get("id");
				deleteHash = (String) elem.get("deletehash");
			}
		}
				
		if(pictureId != null) {
			url = String.format("https://api.imgur.com/3/image/%s", deleteHash);
			albumsReq = new OAuthRequest(Verb.DELETE,
					url, service);
			service.signRequest(accessToken, albumsReq);
			albumsReq.send();
		}	
		
		url = String.format("https://api.imgur.com/3/upload");
		albumsReq = new OAuthRequest(Verb.POST,
				url, service);
			albumsReq.addPayload(data);
			albumsReq.addQuerystringParameter("album", albumId);
			albumsReq.addQuerystringParameter("name", picture);
		service.signRequest(accessToken, albumsReq);
		albumsReq.send();
		
	}
	
	
	
	
	

}
