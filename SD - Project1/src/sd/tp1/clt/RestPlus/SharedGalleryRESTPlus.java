package sd.tp1.clt.RestPlus;

import javafx.application.Application;
import javafx.stage.Stage;
import sd.tp1.gui.impl.GalleryWindow;

/*
 * Launches the local shared gallery application.
 */
public class SharedGalleryRESTPlus extends Application {

	GalleryWindow window;
	
	public SharedGalleryRESTPlus() throws Exception {
		window = new GalleryWindow( new SharedGalleryContentProviderRESTPlus());
	}	
	
	
    public static void main(String[] args){
        launch(args);
    }
    
	@Override
	public void start(Stage primaryStage) throws Exception {
		window.start(primaryStage);
	}
}
