package sd.tp1.clt.RestPlus;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

import sd.tp1.clt.ws.ExistentPictureException_Exception;
import sd.tp1.clt.ws.ExistingAlbumException_Exception;
import sd.tp1.clt.ws.IOException_Exception;
import sd.tp1.clt.ws.InexistentAlbumException_Exception;
import sd.tp1.clt.ws.InexistentPictureException_Exception;
import sd.tp1.clt.ws.SharedServer;
import sd.tp1.clt.ws.SharedServerService;
import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.Gui;
import sd.tp1.gui.GalleryContentProvider.Album;
import sd.tp1.gui.GalleryContentProvider.Picture;
import sd.tp1.clt.Messaging.ClientMessaging;


public class SharedGalleryContentProviderRESTPlus implements GalleryContentProvider {
	
		Map<String, SharedServer> wsdlServers;
		Map<String, WebTarget> restServers;
		Map<String, Long> serverResponse;
	
	
		//cache stuff
		private File cache;
		private long galleryUpdate;
		private Map<String, Long> pictureUpdate;
		private Map<String, List<Picture>> albumContents;
		private Map<String, Long> albumUpdate;
		
		private Gui gui;
			
		//public static final long UPDATE_INTERVAL = 5000;
		public static final int ATEMPTS = 100;
		public static final String MULTICAST_GROUP = "228.5.6.7";
		public static final int MULTICAST_PORT = 6789;
		public static final int TIMEOUT = 1000;
		public static final int BUFFER_SIZE = 65536;
		public static final String URL = "http://%s:%s/Gallery";
		public static final long REQUEST_INTERVAL = 5000;
		public static final long DELETE_SERVER = 10000;

		private static final long UPDATE_ALBUM = 5000;
		private static final long UPDATE_PICTURE = 20000;
		private static final long UPDATE_CONTENT = 10000;

	

	
	public SharedGalleryContentProviderRESTPlus() {
				//multiserver stuff
				wsdlServers = new ConcurrentHashMap<String, SharedServer>(); //here we store the WSDL servers
				restServers = new ConcurrentHashMap<String, WebTarget>();    //here we store the REST servers
				serverResponse = new ConcurrentHashMap<String, Long>();      //here we store the last time a server replied to a "Looking" query
				
				//cache stuff
				galleryUpdate = 0;
				pictureUpdate = new HashMap<String, Long>();
				albumContents = new HashMap<String, List<Picture>>();
				albumUpdate = new HashMap<String, Long>();
				cache = new File(".", "Cache");
				if(!cache.exists())
					cache.mkdir();
				
				//start server listener
				Thread serverListener = new Thread(() -> {
					MulticastSocket cast = null;
					InetAddress group = null;
					long lastRequest = Long.MIN_VALUE;
					try {
						group = InetAddress.getByName(MULTICAST_GROUP);
						cast = new MulticastSocket(MULTICAST_PORT);
						cast.setSoTimeout(TIMEOUT);
						cast.joinGroup(group);
					} catch (Exception e1) {
						System.err.println("Server discovery has quit. " + e1.getMessage());
						return;
					}
					
					
					while(true) {
						
						
						if(lastRequest + REQUEST_INTERVAL < System.currentTimeMillis()) {
							lastRequest = System.currentTimeMillis();
							//System.err.println("Listening for servers...");
							byte[] b = String.format("%s", "Looking").getBytes();
							DatagramPacket p = new DatagramPacket(b, b.length, group, MULTICAST_PORT);
							try {
								cast.send(p);
							} catch (Exception e) {
								//System.err.println(e.getMessage());
								continue;
							}
							
						} 
						
						byte[] buf = new byte[BUFFER_SIZE];
						DatagramPacket r = new DatagramPacket(buf, buf.length);
						try {
							
							cast.receive(r);
							
							
							System.out.println("Recieving: " + r.getAddress().getHostAddress() + ":" + r.getPort());						
							
							String val = new String(r.getData(), 0, r.getLength());
							String[] message = val.split(" ");
							
							System.out.println("Server type: " + val);
							if(message[0].equals("WSDL")) {
								String url = String.format(URL, r.getAddress().getHostAddress(), message[1]);
								System.out.println("URL: " + url);
								if(!wsdlServers.containsKey(url)) {
									SharedServerService serv;
									serv = new SharedServerService(new URL(url));
									SharedServer s = serv.getSharedServerPort();
									wsdlServers.put(url, s);
									System.out.println("Connected with server @ " + url);
								} else System.out.println("Already connected with " + url);
								serverResponse.put(url, System.currentTimeMillis());
							} else if(message[0].equals("REST")) {
								String url = String.format(URL, r.getAddress().getHostAddress(), message[1]);
								System.out.println("URL: " + url);
								if(!restServers.containsKey(url)) {
									ClientConfig config = new ClientConfig();
									Client client = ClientBuilder.newClient(config);
									WebTarget target = client.target(url);
									restServers.put(url, target);
									System.out.println("Connected with server @ " + url);
								} else System.out.println("Already connected with " + url);
								serverResponse.put(url, System.currentTimeMillis());
							} else System.out.println("Unsuported server type: " + val);
							
						}  catch (java.net.SocketTimeoutException time) {
							System.err.println("Timed out..");
						}  catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					
					
				});
				serverListener.start();
				
				//start server remover
				Thread removeServer = new Thread(() -> {
					
					for(Entry<String, Long> e : serverResponse.entrySet()) {
						Long time = e.getValue();
						if(time < System.currentTimeMillis() - DELETE_SERVER) {					
							serverResponse.remove(e.getKey());
							if(wsdlServers.containsKey(e.getKey()))
								wsdlServers.remove(e.getKey());
							if(restServers.containsKey(e.getKey()))
								restServers.remove(e.getKey());
							
							try {
								wait(1000);
							} catch (Exception e1) {
							}
						}
					}
					
				});
				removeServer.start();
				
				//ClientMessaging mes = new ClientMessaging(this.gui);
				//mes.start();
				
				while(wsdlServers.isEmpty() && restServers.isEmpty()) {
					
				}
	}
	

	@Override
	public void register(Gui gui) {
		if( this.gui == null ) {
			this.gui = gui;
		}
	}

	@Override
	public List<Album> getListOfAlbums() {
		List<Album> lst = new ArrayList<Album>();
		if(System.currentTimeMillis() < this.galleryUpdate + UPDATE_ALBUM) {
			for(String s : cache.list())
				lst.add(new SharedAlbum(s));
		} else {
		
			this.galleryUpdate = System.currentTimeMillis();		
			for(SharedServer s : wsdlServers.values())
				for(String a: s.getAlbums()){
					if(!lst.contains(a)) {
						lst.add(new SharedAlbum(a));
					} else System.out.println("Already has a album named " + a);
				}
			
			for(WebTarget t : restServers.values()){
				List<String> l = null;
				Response r = t.request(MediaType.APPLICATION_JSON).get();
				if(r.getStatus() != Status.OK.getStatusCode()) {
					System.out.println(r.getStatus() + " -> " + Status.OK.getStatusCode());
					return null;
				}
				
				l = r.readEntity(ArrayList.class);
				for(String s : l)
					if(!lst.contains(s))
						lst.add(new SharedAlbum(s));
					else System.out.println("Already has a album named " + s);
			}
			
		}
		
		return lst;
	}

	@Override
	public List<Picture> getListOfPictures(Album album) {
		// TODO: obtain remote information 
		
		if(this.albumUpdate.containsKey(album.getName()) 
				&& albumUpdate.get(album.getName()) + UPDATE_CONTENT > System.currentTimeMillis()) {
			return this.albumContents.get(album.getName());
		}
		
		List<Picture> lst = new ArrayList<Picture>();
		for(SharedServer s : wsdlServers.values()) {
			try {
				
					for(String a: s.getListOfPictures(album.getName())){
						if(!lst.contains(a))
							lst.add( new SharedPicture(a));
						else System.err.println("Duplicated picture " + a);
					}
			} catch (InexistentAlbumException_Exception e) {
				continue;
			} catch (IOException_Exception e) {
				continue;
			}
		}
		for(WebTarget t : restServers.values()){
			List<String> l = null;
			Response r = t.path("/ListPictures/" + album.getName()).request().accept(MediaType.APPLICATION_JSON).get();
			if(r.getStatus() != Status.OK.getStatusCode()) {
				continue;
			} 	
			l = r.readEntity(ArrayList.class);
			lst = new ArrayList<Picture>();
			for(String s : l)
				if(!lst.contains(s))
					lst.add(new SharedPicture(s));
				else System.err.println("Duplicated picture " + s);
		}
		
		albumContents.put(album.getName(), lst);
		albumUpdate.put(album.getName(), System.currentTimeMillis());
		
		return lst;
	}

	/**
	 * Returns the contents of picture in album.
	 * On error this method should return null.
	 */
	@Override 
	public byte[] getPictureData(Album album, Picture picture) {
		
		String key = album.getName() + "/" + picture.getName();
		File a = new File(cache.toPath().toString(), album.getName());
		if(a.exists() && this.pictureUpdate.containsKey(key) && pictureUpdate.get(key) + UPDATE_PICTURE < System.currentTimeMillis()) {
			File p = new File(a.toPath().toString(), picture.getName());
			if(p.exists()) {
				try {
					//System.out.println("Reading " + p.getName());
					return Files.readAllBytes(p.toPath());
				} catch (IOException e) {
					//if it fails, we'll try to fetch it from the server
				}
			}
		
		} else a.mkdir();
		
		try {
			
			for(SharedServer s : wsdlServers.values()) {
				try {
					byte[] data = s.getPictureData(album.getName(), picture.getName());
					File p = new File(a.toPath().toString(), picture.getName());
					OutputStream out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
					out.write(data);
					pictureUpdate.put(key, System.currentTimeMillis());
					return data;
				} catch (InexistentAlbumException_Exception | InexistentPictureException_Exception e) {
					System.err.println("SOAP: Couldn't find album or picture.");
					continue;
				} catch (IOException e) {
					continue;
				}
				
			}
			for(WebTarget t : restServers.values()){
				Response r = t.path("/get/"+ album.getName() +"/" + picture.getName()).request().get();
				if(r.getStatus() != Status.OK.getStatusCode()) { 
					System.err.println("REST error " + r.getStatus());					
					continue;
				}
				byte[] b = r.readEntity(byte[].class);
				File p = new File(a.toPath().toString(), picture.getName());
				OutputStream out;
				try {
					out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
					out.write(b);
				} catch (IOException e) {
					
				}
				pictureUpdate.put(key, System.currentTimeMillis());
			    return b;
			}
						//System.out.println("Data size: " + data.length);
			return null;
		} catch (IOException_Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	
	/**
	 * Create a new album.
	 * On error this method should return null.
	 */
	@Override 
	public Album createAlbum(String name) {
	
		try {
			Random generator = new Random();
			Object[] values = serverResponse.keySet().toArray();
			String url = (String) values[generator.nextInt(values.length)];
			
			if(wsdlServers.containsKey(url)) {
				wsdlServers.get(url).createAlbum(name);
			}  else if (restServers.containsKey(url)) {
				WebTarget t = restServers.get(url);
				Response r = t.path("/CreateAlbum").request().post(Entity.entity(name, MediaType.APPLICATION_OCTET_STREAM));
				if(r.getStatus() != Status.OK.getStatusCode()) return null;
				this.galleryUpdate = Long.MIN_VALUE;
			}
			this.galleryUpdate = Long.MIN_VALUE;
		
		} catch (ExistingAlbumException_Exception
				| IOException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return new SharedAlbum(name);
		
	}

	/**
	 * Delete an existing album.
	 */
	@Override 
	public void deleteAlbum(Album album) {
		
		for(SharedServer s : wsdlServers.values()) {
			try {
				//System.out.println("Deleting album: " + album.getName());
				s.deleteAlbum(album.getName());
			} catch (InexistentAlbumException_Exception | IOException_Exception e) {
				// TODO Auto-generated catch block
			}
		}
		for(WebTarget t : restServers.values())
			t.path("/DeleteAlbum/" + album.getName()).request().delete();
		this.galleryUpdate = Long.MIN_VALUE;
	}
	
	/**
	 * Add a new picture to an album.
	 * On error this method should return null.
	 */
	@Override 
	public Picture uploadPicture(Album album, String name, byte[] data) {
		// TODO: contact servers to add picture name with contents data
		boolean found = false;
		for(SharedServer s : wsdlServers.values()) {
			try {
				s.uploadPicture(album.getName(), name, data);
				found = true;
			} catch (InexistentAlbumException_Exception | ExistentPictureException_Exception | IOException_Exception e) {
				
			}
		}
		for(WebTarget t : restServers.values()){
			System.out.println("Uploading " + name);
			Response r = t.path("/Upload/" + album.getName() + "/" + name).request().post(Entity.entity(data, MediaType.APPLICATION_OCTET_STREAM));
			if(r.getStatus() != Status.OK.getStatusCode())	{	
				System.out.println(r.getStatus() + " / " + Status.OK.getStatusCode());
				return null; 
			} else {
				found = true;
				//System.out.println("Sent " + name);
			}
		}
		albumUpdate.put(album.getName(), Long.MIN_VALUE);
		if(found)
			return new SharedPicture(name);
		else return null;
	}

	/**
	 * Delete a picture from an album.
	 * On error this method should return false.
	 */
	@Override 
	public boolean deletePicture(Album album, Picture picture) {
		
		boolean found = false;
		for(SharedServer s : wsdlServers.values()) {
			try {
				s.deletePicture(album.getName(), picture.getName());
				found = true;
			} catch (InexistentAlbumException_Exception | IOException_Exception | InexistentPictureException_Exception e) {
				
			}
		}
		for(WebTarget t : restServers.values()){
			Response r = t.path("/DeletePicture/" + album.getName() + "/" + picture.getName()).request().delete();
			if(r.getStatus() == Status.OK.getStatusCode())
				found = true;
		}
		albumUpdate.put(album.getName(), Long.MIN_VALUE);
		return found;
	}

	
	/**
	 * Represents a shared album.
	 */
	static class SharedAlbum implements GalleryContentProvider.Album {
		final String name;

		SharedAlbum(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof String) {
				return ((String)obj).equals(this.name);
			}
			
			if(obj instanceof SharedAlbum) {
				return ((SharedAlbum) obj).getName().equals(this.name);
			}
			return false;
			
		}
	}

	/**
	 * Represents a shared picture.
	 */
	static class SharedPicture implements GalleryContentProvider.Picture {
		final String name;

		SharedPicture(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof String) {
				return ((String)obj).equals(this.name);
			}
			
			if(obj instanceof SharedPicture) {
				return ((SharedPicture) obj).getName().equals(this.name);
			}
			return false;
			
		}
	}
}
