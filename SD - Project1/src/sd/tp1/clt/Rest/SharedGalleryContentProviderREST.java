package sd.tp1.clt.Rest;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

import sd.tp1.clt.ws.SharedServer;
import sd.tp1.clt.ws.SharedServerService;
import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.Gui;
import sd.tp1.gui.GalleryContentProvider.Picture;
import sd.tp1.srv.Rest.SharedServerREST;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;



/*
 * This class provides the album/picture content to the gui/main application.
 * 
 * Project 1 implementation should complete this class. 
 */
public class SharedGalleryContentProviderREST implements GalleryContentProvider{

	Gui gui;
	
	private Client client;
	private WebTarget target;
	private Map<String, WebTarget> servers;
	private Map<String, Long> serverResponse;
	
	private File cache;
	private long galleryUpdate;
	private Map<String, Long> pictureUpdate;
	private Map<String, List<Picture>> albumContents;
	private Map<String, Long> albumUpdate;
	
	public static final int ATEMPTS = 100;
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int TIMEOUT = 5000;
	public static final int BUFFER_SIZE = 65536;
	public static final long REQUEST_INTERVAL = 15000;
	public static final long DELETE_SERVER = 30000;
	public static final String URL = "http://%s:8080/Gallery";
	private static final long UPDATE_ALBUM = 5000;
	private static final long UPDATE_PICTURE = 60000;
	private static final long UPDATE_CONTENT = 15000;

	SharedGalleryContentProviderREST() throws URISyntaxException {
		// TODO: code to do when shared gallery starts
		servers = new ConcurrentHashMap<String, WebTarget>();
		serverResponse = new ConcurrentHashMap<String, Long>();
		
		galleryUpdate = 0;
		pictureUpdate = new HashMap<String, Long>();
		albumContents = new HashMap<String, List<Picture>>();
		albumUpdate = new HashMap<String, Long>();
		cache = new File(".", "Cache");
		if(!cache.exists())
			cache.mkdir();
		
		Thread serverListener = new Thread(()->{
			
			MulticastSocket cast = null;
			InetAddress group = null;
			long lastRequest = 0;
			try {
				group = InetAddress.getByName(MULTICAST_GROUP);
				cast = new MulticastSocket(MULTICAST_PORT);
				cast.setSoTimeout(TIMEOUT);
				cast.joinGroup(group);
			} catch (Exception e1) {
				//System.out.println("Server discovery has quit. " + e1.getMessage());
				return;
			}
			
			while(true) {
				
				
				if(lastRequest + REQUEST_INTERVAL < System.currentTimeMillis() ) {
					lastRequest = System.currentTimeMillis();
					//System.err.println("Listening for servers...");
					byte[] b = String.format("%s", "Looking").getBytes();
					DatagramPacket p = new DatagramPacket(b, b.length, group, MULTICAST_PORT);
					try {
						cast.send(p);
					} catch (Exception e) {
						//System.out.println(e.getMessage());
						continue;
					}
				}
				
				byte[] buf = new byte[BUFFER_SIZE];
				DatagramPacket r = new DatagramPacket(buf, buf.length);
				try {
					try {
						cast.receive(r);
					} catch (Exception e) {
						continue; //NOTA: adicionado posterior a entrega
					}
					
					//System.out.println("Recieving: " + r.getAddress().getHostAddress() + ":" + r.getPort());
					String url = String.format(URL, r.getAddress().getHostAddress());					
					//System.out.println(url);
					
					String type = new String(r.getData(), 0, r.getLength());
					//System.out.println("Recieved: " + type);
					
					if(type.equals("REST")) {
						if(!servers.containsKey(url)) {
							ClientConfig config = new ClientConfig();
							client = ClientBuilder.newClient(config);
							target = client.target(url);
							servers.put(url, target);
							//System.err.println("Connected with server @ " + url);
						} /*else System.err.println("Already connected with " + url);*/
						serverResponse.put(url, System.currentTimeMillis());
					} /*else System.err.println("Unsuported server type: " + type);*/
					
					//wait(1000);
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			
			
		});
		serverListener.start();
		
		
		Thread removeServer = new Thread(() -> {
			
			for(Entry<String, Long> e : serverResponse.entrySet()) {
				Long time = e.getValue();
				if(time < System.currentTimeMillis() + DELETE_SERVER) {					
					servers.remove(e.getKey());
					serverResponse.remove(e.getKey());
					try {
						wait(1000);
					} catch (Exception e1) {
					}
				}
			}
			
		});
		removeServer.start();
		
		while(servers.isEmpty()) {
			
		}
			
			
	}
	
	
		
	/**
	 *  Downcall from the GUI to register itself, so that it can be updated via upcalls.
	 */
	@Override
	public void register(Gui gui) {
		if( this.gui == null ) {
			this.gui = gui;
		}
	}

	/**
	 * Returns the list of albums in the system.
	 * On error this method should return null.
	 */
	@Override 
	public List<Album> getListOfAlbums() {
		List<Album> a = new ArrayList<Album>();
		List<String> l = null;
		if(System.currentTimeMillis() < this.galleryUpdate + UPDATE_ALBUM) {
			for(String s : cache.list())
				a.add(new SharedAlbum(s));
			return a;
		} 
		
		this.galleryUpdate = System.currentTimeMillis();		
		for(WebTarget t : servers.values()){
			Response r = t.request(MediaType.APPLICATION_JSON).get();
			if(r.getStatus() != Status.OK.getStatusCode()) {
				System.out.println(r.getStatus() + " -> " + Status.OK.getStatusCode());
				return null;
			}
			
			l = r.readEntity(ArrayList.class);
	
			for(String s : l)
				a.add(new SharedAlbum(s));
		}
		return a;
	}

	/**
	 * Returns the list of pictures for the given album. 
	 * On error this method should return null.
	 */
	@Override 
	public List<Picture> getListOfPictures(Album album) {
		if(this.albumUpdate.containsKey(album.getName()) 
				&& albumUpdate.get(album.getName()) + UPDATE_CONTENT > System.currentTimeMillis()) {
			return this.albumContents.get(album.getName());
		}
		List<Picture> a = null;
		for(WebTarget t : servers.values()){
			List<String> l = null;
			Response r = t.path("/ListPictures/" + album.getName()).request().accept(MediaType.APPLICATION_JSON).get();
			if(r.getStatus() != Status.OK.getStatusCode()) return null;		
			l = r.readEntity(ArrayList.class);
			a = new ArrayList<Picture>();
			for(String s : l)
				a.add(new SharedPicture(s));
		}
		albumContents.put(album.getName(), a);
		albumUpdate.put(album.getName(), System.currentTimeMillis());
		return a;
	}

	/**
	 * Returns the contents of picture in album.
	 * On error this method should return null.
	 */
	@Override 
	public byte[] getPictureData(Album album, Picture picture) {
		String key = album.getName() + "/" + picture.getName();
		File a = new File(cache.toPath().toString(), album.getName());
		if(a.exists() && this.pictureUpdate.containsKey(key) && pictureUpdate.get(key) + UPDATE_PICTURE < System.currentTimeMillis()) {
			File p = new File(a.toPath().toString(), picture.getName());
			if(p.exists()) {
				try {
					//System.out.println("Reading " + p.getName());
					return Files.readAllBytes(p.toPath());
				} catch (IOException e) {
					//if it fails, we'll try to fetch it from the server
				}
			}
		
		} else a.mkdir();
		
		for(WebTarget t : servers.values()){
			Response r = t.path("/PictureData/"+ album.getName() +"/" + picture.getName()).request().get();
			if(r.getStatus() != Status.OK.getStatusCode()) return null;
			byte[] b = r.readEntity(byte[].class);
			File p = new File(a.toPath().toString(), picture.getName());
			OutputStream out;
			try {
				out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
				out.write(b);
			} catch (IOException e) {
				return null;
			}
			pictureUpdate.put(key, System.currentTimeMillis());
			return b;
		}
		return null;
	}
	
	private WebTarget getRandomServer() {
		Random r = new Random();
		float max = 0;
		WebTarget t = null;
		for(WebTarget te : servers.values()) {
			float n = r.nextFloat() + 0.1f;
			if(n > max) {
				t = te;
				max = n;
			}
		}
		return t;
	}

	/**
	 * Create a new album.
	 * On error this method should return null.
	 */
	@Override 
	public Album createAlbum(String name) {
		Response r = getRandomServer().path("/CreateAlbum").request().post(Entity.entity(name, MediaType.APPLICATION_OCTET_STREAM));
		if(r.getStatus() != Status.OK.getStatusCode()) return null;
		this.galleryUpdate = Long.MIN_VALUE;
		return new SharedAlbum(name); //target.request().post(a, MediaType.APPLICATION_JSON);
	}

	/**
	 * Delete an existing album.
	 */
	@Override 
	public void deleteAlbum(Album album) {
		for(WebTarget t : servers.values())
			t.path("/DeleteAlbum/" + album.getName()).request().delete();
		this.galleryUpdate = Long.MIN_VALUE;
	}
	
	/**
	 * Add a new picture to an album.
	 * On error this method should return null.
	 */
	@Override 
	public Picture uploadPicture(Album album, String name, byte[] data) {
		boolean found = false;
		for(WebTarget t : servers.values()){
			//System.out.println("Uploading " + name);
			Response r = t.path("/Upload/" + album.getName() + "/" + name).request().post(Entity.entity(data, MediaType.APPLICATION_OCTET_STREAM));
			if(r.getStatus() != Status.OK.getStatusCode())	{	
				//System.out.println(r.getStatus() + " / " + Status.OK.getStatusCode());
				return null; 
			} else {
				found = true;
				//System.out.println("Sent " + name);
			}
		}
		albumUpdate.put(album.getName(), Long.MIN_VALUE);
		if(found)
			return new SharedPicture(name);
		return null;
	}

	/**
	 * Delete a picture from an album.
	 * On error this method should return false.
	 */
	@Override 
	public boolean deletePicture(Album album, Picture picture) {
		
		boolean deleted = false;
		for(WebTarget t : servers.values()){
			Response r = t.path("/DeletePicture/" + album.getName() + "/" + picture.getName()).request().delete();
			if(r.getStatus() == Status.OK.getStatusCode())
				deleted = true;
		}
		albumUpdate.put(album.getName(), Long.MIN_VALUE);
		return deleted;
	}

	
	/**
	 * Represents a shared album.
	 */
	public static class SharedAlbum implements GalleryContentProvider.Album {
		final String name;

		public SharedAlbum(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
	}

	/**
	 * Represents a shared picture.
	 */
	public static class SharedPicture implements GalleryContentProvider.Picture {
		
		final String name;
		

		public SharedPicture(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
	}
}
