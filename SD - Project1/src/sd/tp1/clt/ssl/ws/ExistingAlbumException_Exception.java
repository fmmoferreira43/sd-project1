
package sd.tp1.clt.ssl.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "ExistingAlbumException", targetNamespace = "http://srv.tp1.sd/")
public class ExistingAlbumException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private ExistingAlbumException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public ExistingAlbumException_Exception(String message, ExistingAlbumException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public ExistingAlbumException_Exception(String message, ExistingAlbumException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: sd.tp1.clt.ssl.ws.ExistingAlbumException
     */
    public ExistingAlbumException getFaultInfo() {
        return faultInfo;
    }

}
