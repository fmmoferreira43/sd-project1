
package sd.tp1.clt.ssl.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sd.tp1.clt.ssl.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LastChangeResponse_QNAME = new QName("http://srv.tp1.sd/", "lastChangeResponse");
    private final static QName _DeleteAlbum_QNAME = new QName("http://srv.tp1.sd/", "deleteAlbum");
    private final static QName _UploadPictureResponse_QNAME = new QName("http://srv.tp1.sd/", "uploadPictureResponse");
    private final static QName _InexistentPictureException_QNAME = new QName("http://srv.tp1.sd/", "InexistentPictureException");
    private final static QName _CreateAlbum_QNAME = new QName("http://srv.tp1.sd/", "createAlbum");
    private final static QName _LastChange_QNAME = new QName("http://srv.tp1.sd/", "lastChange");
    private final static QName _DeletePictureResponse_QNAME = new QName("http://srv.tp1.sd/", "deletePictureResponse");
    private final static QName _GetAlbumsResponse_QNAME = new QName("http://srv.tp1.sd/", "getAlbumsResponse");
    private final static QName _GetListOfPictures_QNAME = new QName("http://srv.tp1.sd/", "getListOfPictures");
    private final static QName _GetListOfPicturesResponse_QNAME = new QName("http://srv.tp1.sd/", "getListOfPicturesResponse");
    private final static QName _GetAlbums_QNAME = new QName("http://srv.tp1.sd/", "getAlbums");
    private final static QName _UploadPicture_QNAME = new QName("http://srv.tp1.sd/", "uploadPicture");
    private final static QName _InexistentAlbumException_QNAME = new QName("http://srv.tp1.sd/", "InexistentAlbumException");
    private final static QName _DirectorySize_QNAME = new QName("http://srv.tp1.sd/", "directorySize");
    private final static QName _GetPictureData_QNAME = new QName("http://srv.tp1.sd/", "getPictureData");
    private final static QName _GetPictureDataResponse_QNAME = new QName("http://srv.tp1.sd/", "getPictureDataResponse");
    private final static QName _NotFileServerException_QNAME = new QName("http://srv.tp1.sd/", "NotFileServerException");
    private final static QName _ExistingAlbumException_QNAME = new QName("http://srv.tp1.sd/", "ExistingAlbumException");
    private final static QName _DeleteAlbumResponse_QNAME = new QName("http://srv.tp1.sd/", "deleteAlbumResponse");
    private final static QName _CreateAlbumResponse_QNAME = new QName("http://srv.tp1.sd/", "createAlbumResponse");
    private final static QName _DirectorySizeResponse_QNAME = new QName("http://srv.tp1.sd/", "directorySizeResponse");
    private final static QName _IOException_QNAME = new QName("http://srv.tp1.sd/", "IOException");
    private final static QName _ExistentPictureException_QNAME = new QName("http://srv.tp1.sd/", "ExistentPictureException");
    private final static QName _DeletePicture_QNAME = new QName("http://srv.tp1.sd/", "deletePicture");
    private final static QName _UploadPictureArg2_QNAME = new QName("", "arg2");
    private final static QName _GetPictureDataResponseReturn_QNAME = new QName("", "return");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sd.tp1.clt.ssl.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExistingAlbumException }
     * 
     */
    public ExistingAlbumException createExistingAlbumException() {
        return new ExistingAlbumException();
    }

    /**
     * Create an instance of {@link DeleteAlbumResponse }
     * 
     */
    public DeleteAlbumResponse createDeleteAlbumResponse() {
        return new DeleteAlbumResponse();
    }

    /**
     * Create an instance of {@link NotFileServerException }
     * 
     */
    public NotFileServerException createNotFileServerException() {
        return new NotFileServerException();
    }

    /**
     * Create an instance of {@link InexistentAlbumException }
     * 
     */
    public InexistentAlbumException createInexistentAlbumException() {
        return new InexistentAlbumException();
    }

    /**
     * Create an instance of {@link DirectorySize }
     * 
     */
    public DirectorySize createDirectorySize() {
        return new DirectorySize();
    }

    /**
     * Create an instance of {@link GetPictureData }
     * 
     */
    public GetPictureData createGetPictureData() {
        return new GetPictureData();
    }

    /**
     * Create an instance of {@link GetPictureDataResponse }
     * 
     */
    public GetPictureDataResponse createGetPictureDataResponse() {
        return new GetPictureDataResponse();
    }

    /**
     * Create an instance of {@link GetAlbums }
     * 
     */
    public GetAlbums createGetAlbums() {
        return new GetAlbums();
    }

    /**
     * Create an instance of {@link UploadPicture }
     * 
     */
    public UploadPicture createUploadPicture() {
        return new UploadPicture();
    }

    /**
     * Create an instance of {@link DeletePicture }
     * 
     */
    public DeletePicture createDeletePicture() {
        return new DeletePicture();
    }

    /**
     * Create an instance of {@link ExistentPictureException }
     * 
     */
    public ExistentPictureException createExistentPictureException() {
        return new ExistentPictureException();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link DirectorySizeResponse }
     * 
     */
    public DirectorySizeResponse createDirectorySizeResponse() {
        return new DirectorySizeResponse();
    }

    /**
     * Create an instance of {@link CreateAlbumResponse }
     * 
     */
    public CreateAlbumResponse createCreateAlbumResponse() {
        return new CreateAlbumResponse();
    }

    /**
     * Create an instance of {@link UploadPictureResponse }
     * 
     */
    public UploadPictureResponse createUploadPictureResponse() {
        return new UploadPictureResponse();
    }

    /**
     * Create an instance of {@link DeleteAlbum }
     * 
     */
    public DeleteAlbum createDeleteAlbum() {
        return new DeleteAlbum();
    }

    /**
     * Create an instance of {@link LastChangeResponse }
     * 
     */
    public LastChangeResponse createLastChangeResponse() {
        return new LastChangeResponse();
    }

    /**
     * Create an instance of {@link DeletePictureResponse }
     * 
     */
    public DeletePictureResponse createDeletePictureResponse() {
        return new DeletePictureResponse();
    }

    /**
     * Create an instance of {@link GetAlbumsResponse }
     * 
     */
    public GetAlbumsResponse createGetAlbumsResponse() {
        return new GetAlbumsResponse();
    }

    /**
     * Create an instance of {@link GetListOfPictures }
     * 
     */
    public GetListOfPictures createGetListOfPictures() {
        return new GetListOfPictures();
    }

    /**
     * Create an instance of {@link GetListOfPicturesResponse }
     * 
     */
    public GetListOfPicturesResponse createGetListOfPicturesResponse() {
        return new GetListOfPicturesResponse();
    }

    /**
     * Create an instance of {@link CreateAlbum }
     * 
     */
    public CreateAlbum createCreateAlbum() {
        return new CreateAlbum();
    }

    /**
     * Create an instance of {@link LastChange }
     * 
     */
    public LastChange createLastChange() {
        return new LastChange();
    }

    /**
     * Create an instance of {@link InexistentPictureException }
     * 
     */
    public InexistentPictureException createInexistentPictureException() {
        return new InexistentPictureException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LastChangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "lastChangeResponse")
    public JAXBElement<LastChangeResponse> createLastChangeResponse(LastChangeResponse value) {
        return new JAXBElement<LastChangeResponse>(_LastChangeResponse_QNAME, LastChangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAlbum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "deleteAlbum")
    public JAXBElement<DeleteAlbum> createDeleteAlbum(DeleteAlbum value) {
        return new JAXBElement<DeleteAlbum>(_DeleteAlbum_QNAME, DeleteAlbum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadPictureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "uploadPictureResponse")
    public JAXBElement<UploadPictureResponse> createUploadPictureResponse(UploadPictureResponse value) {
        return new JAXBElement<UploadPictureResponse>(_UploadPictureResponse_QNAME, UploadPictureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InexistentPictureException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "InexistentPictureException")
    public JAXBElement<InexistentPictureException> createInexistentPictureException(InexistentPictureException value) {
        return new JAXBElement<InexistentPictureException>(_InexistentPictureException_QNAME, InexistentPictureException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAlbum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "createAlbum")
    public JAXBElement<CreateAlbum> createCreateAlbum(CreateAlbum value) {
        return new JAXBElement<CreateAlbum>(_CreateAlbum_QNAME, CreateAlbum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LastChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "lastChange")
    public JAXBElement<LastChange> createLastChange(LastChange value) {
        return new JAXBElement<LastChange>(_LastChange_QNAME, LastChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePictureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "deletePictureResponse")
    public JAXBElement<DeletePictureResponse> createDeletePictureResponse(DeletePictureResponse value) {
        return new JAXBElement<DeletePictureResponse>(_DeletePictureResponse_QNAME, DeletePictureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAlbumsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getAlbumsResponse")
    public JAXBElement<GetAlbumsResponse> createGetAlbumsResponse(GetAlbumsResponse value) {
        return new JAXBElement<GetAlbumsResponse>(_GetAlbumsResponse_QNAME, GetAlbumsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfPictures }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getListOfPictures")
    public JAXBElement<GetListOfPictures> createGetListOfPictures(GetListOfPictures value) {
        return new JAXBElement<GetListOfPictures>(_GetListOfPictures_QNAME, GetListOfPictures.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfPicturesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getListOfPicturesResponse")
    public JAXBElement<GetListOfPicturesResponse> createGetListOfPicturesResponse(GetListOfPicturesResponse value) {
        return new JAXBElement<GetListOfPicturesResponse>(_GetListOfPicturesResponse_QNAME, GetListOfPicturesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAlbums }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getAlbums")
    public JAXBElement<GetAlbums> createGetAlbums(GetAlbums value) {
        return new JAXBElement<GetAlbums>(_GetAlbums_QNAME, GetAlbums.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadPicture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "uploadPicture")
    public JAXBElement<UploadPicture> createUploadPicture(UploadPicture value) {
        return new JAXBElement<UploadPicture>(_UploadPicture_QNAME, UploadPicture.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InexistentAlbumException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "InexistentAlbumException")
    public JAXBElement<InexistentAlbumException> createInexistentAlbumException(InexistentAlbumException value) {
        return new JAXBElement<InexistentAlbumException>(_InexistentAlbumException_QNAME, InexistentAlbumException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectorySize }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "directorySize")
    public JAXBElement<DirectorySize> createDirectorySize(DirectorySize value) {
        return new JAXBElement<DirectorySize>(_DirectorySize_QNAME, DirectorySize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPictureData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getPictureData")
    public JAXBElement<GetPictureData> createGetPictureData(GetPictureData value) {
        return new JAXBElement<GetPictureData>(_GetPictureData_QNAME, GetPictureData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPictureDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "getPictureDataResponse")
    public JAXBElement<GetPictureDataResponse> createGetPictureDataResponse(GetPictureDataResponse value) {
        return new JAXBElement<GetPictureDataResponse>(_GetPictureDataResponse_QNAME, GetPictureDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotFileServerException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "NotFileServerException")
    public JAXBElement<NotFileServerException> createNotFileServerException(NotFileServerException value) {
        return new JAXBElement<NotFileServerException>(_NotFileServerException_QNAME, NotFileServerException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistingAlbumException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "ExistingAlbumException")
    public JAXBElement<ExistingAlbumException> createExistingAlbumException(ExistingAlbumException value) {
        return new JAXBElement<ExistingAlbumException>(_ExistingAlbumException_QNAME, ExistingAlbumException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAlbumResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "deleteAlbumResponse")
    public JAXBElement<DeleteAlbumResponse> createDeleteAlbumResponse(DeleteAlbumResponse value) {
        return new JAXBElement<DeleteAlbumResponse>(_DeleteAlbumResponse_QNAME, DeleteAlbumResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAlbumResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "createAlbumResponse")
    public JAXBElement<CreateAlbumResponse> createCreateAlbumResponse(CreateAlbumResponse value) {
        return new JAXBElement<CreateAlbumResponse>(_CreateAlbumResponse_QNAME, CreateAlbumResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectorySizeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "directorySizeResponse")
    public JAXBElement<DirectorySizeResponse> createDirectorySizeResponse(DirectorySizeResponse value) {
        return new JAXBElement<DirectorySizeResponse>(_DirectorySizeResponse_QNAME, DirectorySizeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistentPictureException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "ExistentPictureException")
    public JAXBElement<ExistentPictureException> createExistentPictureException(ExistentPictureException value) {
        return new JAXBElement<ExistentPictureException>(_ExistentPictureException_QNAME, ExistentPictureException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePicture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://srv.tp1.sd/", name = "deletePicture")
    public JAXBElement<DeletePicture> createDeletePicture(DeletePicture value) {
        return new JAXBElement<DeletePicture>(_DeletePicture_QNAME, DeletePicture.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "arg2", scope = UploadPicture.class)
    public JAXBElement<byte[]> createUploadPictureArg2(byte[] value) {
        return new JAXBElement<byte[]>(_UploadPictureArg2_QNAME, byte[].class, UploadPicture.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = GetPictureDataResponse.class)
    public JAXBElement<byte[]> createGetPictureDataResponseReturn(byte[] value) {
        return new JAXBElement<byte[]>(_GetPictureDataResponseReturn_QNAME, byte[].class, GetPictureDataResponse.class, ((byte[]) value));
    }

}
