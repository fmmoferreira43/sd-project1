package sd.tp1.clt;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import sd.tp1.clt.ws.ExistentPictureException_Exception;
import sd.tp1.clt.ws.ExistingAlbumException_Exception;
import sd.tp1.clt.ws.IOException_Exception;
import sd.tp1.clt.ws.InexistentAlbumException_Exception;
import sd.tp1.clt.ws.InexistentPictureException_Exception;
import sd.tp1.clt.ws.SharedServer;
import sd.tp1.clt.ws.SharedServerService;
import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.Gui;


/*
 * This class provides the album/picture content to the gui/main application.
 * 
 * Project 1 implementation should complete this class. 
 */
public class SharedGalleryContentProvider implements GalleryContentProvider{

	Gui gui;
	
	//being able to support multiple servers
	private Map<String, SharedServer> servers;
	private Map<String, Long> serverResponse;
	
	//cache stuff
	private File cache;
	private long galleryUpdate;
	private Map<String, Long> pictureUpdate;
	private Map<String, List<Picture>> albumContents;
	private Map<String, Long> albumUpdate;
		
	
	//public static final long UPDATE_INTERVAL = 5000;
	public static final int ATEMPTS = 100;
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int TIMEOUT = 1000;
	public static final int BUFFER_SIZE = 65536;
	public static final String URL = "http://%s:8080/Gallery";
	public static final long REQUEST_INTERVAL = 1000;
	public static final long DELETE_SERVER = 5000;

	private static final long UPDATE_ALBUM = 5000;
	private static final long UPDATE_PICTURE = 60000;
	private static final long UPDATE_CONTENT = 15000;

	SharedGalleryContentProvider() {
		//multiserver stuff
		servers = new ConcurrentHashMap<String, SharedServer>();
		serverResponse = new ConcurrentHashMap<String, Long>();
		
		//cache stuff
		galleryUpdate = 0;
		pictureUpdate = new HashMap<String, Long>();
		albumContents = new HashMap<String, List<Picture>>();
		albumUpdate = new HashMap<String, Long>();
		cache = new File(".", "Cache");
		if(!cache.exists())
			cache.mkdir();
		
		//start server listener
		Thread serverListener = new Thread(() -> {
			MulticastSocket cast = null;
			InetAddress group = null;
			long lastRequest = Long.MIN_VALUE;
			try {
				group = InetAddress.getByName(MULTICAST_GROUP);
				cast = new MulticastSocket(MULTICAST_PORT);
				cast.setSoTimeout(TIMEOUT);
				cast.joinGroup(group);
			} catch (Exception e1) {
				System.out.println("Server discovery has quit. " + e1.getMessage());
				return;
			}
			
			boolean sent = false;
			while(true) {
				
				
				if(lastRequest + REQUEST_INTERVAL < System.currentTimeMillis()) {
					lastRequest = System.currentTimeMillis();
					System.out.println("Listening for servers...");
					byte[] b = String.format("%s", "Looking").getBytes();
					DatagramPacket p = new DatagramPacket(b, b.length, group, MULTICAST_PORT);
					try {
						cast.send(p);
					} catch (Exception e) {
						System.out.println(e.getMessage());
						continue;
					}
					sent = true;
				} else sent = false;
				
				byte[] buf = new byte[BUFFER_SIZE];
				DatagramPacket r = new DatagramPacket(buf, buf.length);
				try {
					
					cast.receive(r);						
					System.out.println("Recieving: " + r.getAddress().getHostAddress() + ":" + r.getPort());
					String url = String.format(URL, r.getAddress().getHostAddress());					
					System.out.println(url);
					
					String val = new String(r.getData(), 0, r.getLength());
					System.out.println("Server type: " + val);
					if(val.equals("WSDL")) {
						if(val.equals("WSDL") && !servers.containsKey(url)) {
							SharedServerService serv;
							serv = new SharedServerService(new URL(url));
							SharedServer s = serv.getSharedServerPort();
							servers.put(url, s);
							System.out.println("Connected with server @ " + url);
						} else System.err.println("Already connected with " + url);
						serverResponse.put(url, System.currentTimeMillis());
					} else System.err.println("Unsuported server type: " + val);
					
					
					//wait(1000);
					
				}  catch (java.net.SocketTimeoutException time) {
					
				}  catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			
			
		});
		serverListener.start();
		
		//start server remover
		Thread removeServer = new Thread(() -> {
			
			for(Entry<String, Long> e : serverResponse.entrySet()) {
				Long time = e.getValue();
				if(time < System.currentTimeMillis() - DELETE_SERVER) {					
					servers.remove(e.getKey());
					serverResponse.remove(e.getKey());
					try {
						wait(1000);
					} catch (Exception e1) {
					}
				}
			}
			
		});
		removeServer.start();
		
		while(servers.isEmpty()) {
			
		}
		
	}
	
	
		
	/**
	 *  Downcall from the GUI to register itself, so that it can be updated via upcalls.
	 */
	@Override
	public void register(Gui gui) {
		if( this.gui == null ) {
			this.gui = gui;
		}
	}

	/**
	 * Returns the list of albums in the system.
	 * On error this method should return null.
	 */
	@Override 
	public List<Album> getListOfAlbums() {
		// TODO: obtain remote information 
		List<Album> lst = new ArrayList<Album>();
		if(System.currentTimeMillis() < this.galleryUpdate + UPDATE_ALBUM) {
			for(String s : cache.list())
				lst.add(new SharedAlbum(s));
		} else {
		this.galleryUpdate = System.currentTimeMillis();		
		for(SharedServer s : servers.values())
			for(String a: s.getAlbums()){
				if(!lst.contains(a))
					lst.add(new SharedAlbum(a));
			}
		}
		
		return lst;
	}

	/**
	 * Returns the list of pictures for the given album. 
	 * On error this method should return null.
	 */
	@Override 
	public List<Picture> getListOfPictures(Album album) {
		// TODO: obtain remote information 
		
		if(this.albumUpdate.containsKey(album.getName()) 
				&& albumUpdate.get(album.getName()) + UPDATE_CONTENT > System.currentTimeMillis()) {
			return this.albumContents.get(album.getName());
		}
		
		List<Picture> lst = new ArrayList<Picture>();
		try {
			for(SharedServer s : servers.values())
				for(String a: s.getListOfPictures(album.getName())){
					if(!lst.contains(a))
						lst.add( new SharedPicture(a));
				}
		} catch (InexistentAlbumException_Exception e) {
			return null;
		} catch (IOException_Exception e) {
			return null;
		}
		
		albumContents.put(album.getName(), lst);
		albumUpdate.put(album.getName(), System.currentTimeMillis());
		
		return lst;
	}

	/**
	 * Returns the contents of picture in album.
	 * On error this method should return null.
	 */
	@Override 
	public byte[] getPictureData(Album album, Picture picture) {
		
		String key = album.getName() + "/" + picture.getName();
		File a = new File(cache.toPath().toString(), album.getName());
		if(a.exists() && this.pictureUpdate.containsKey(key) && pictureUpdate.get(key) + UPDATE_PICTURE < System.currentTimeMillis()) {
			File p = new File(a.toPath().toString(), picture.getName());
			if(p.exists()) {
				try {
					//System.out.println("Reading " + p.getName());
					return Files.readAllBytes(p.toPath());
				} catch (IOException e) {
					//if it fails, we'll try to fetch it from the server
				}
			}
		
		} else a.mkdir();
		
		try {
			
			for(SharedServer s : servers.values()) {
				try {
					byte[] data = s.getPictureData(album.getName(), picture.getName());
					File p = new File(a.toPath().toString(), picture.getName());
					OutputStream out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
					out.write(data);
					pictureUpdate.put(key, System.currentTimeMillis());
					return data;
				} catch (InexistentAlbumException_Exception | InexistentPictureException_Exception e) {
					//aqui ignora...
				} catch (IOException e) {
					return null;
				}
				
			}
						//System.out.println("Data size: " + data.length);
			return null;
		} catch (IOException_Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	
	private SharedServer getRandomServer() {
		Random r = new Random();
		float max = 0;
		SharedServer s = null;
		for(SharedServer se : servers.values()) {
			float n = r.nextFloat() + 0.1f;
			if(n > max) {
				s = se;
				max = n;
			}
		}
		return s;
	}
	
	/**
	 * Create a new album.
	 * On error this method should return null.
	 */
	@Override 
	public Album createAlbum(String name) {
		try {
			getRandomServer().createAlbum(name);
			this.galleryUpdate = Long.MIN_VALUE;
		} catch (ExistingAlbumException_Exception | IOException_Exception e) {
			return null;
		}
		
		return new SharedAlbum(name);
		
	}

	/**
	 * Delete an existing album.
	 */
	@Override 
	public void deleteAlbum(Album album) {
		
		for(SharedServer s : servers.values()) {
			try {
				//System.out.println("Deleting album: " + album.getName());
				s.deleteAlbum(album.getName());
			} catch (InexistentAlbumException_Exception | IOException_Exception e) {
				// TODO Auto-generated catch block
			}
		}
		this.galleryUpdate = Long.MIN_VALUE;
		albumUpdate.put(album.getName(), Long.MIN_VALUE);
	}
	
	/**
	 * Add a new picture to an album.
	 * On error this method should return null.
	 */
	@Override 
	public Picture uploadPicture(Album album, String name, byte[] data) {
		// TODO: contact servers to add picture name with contents data
		boolean found = false;
		for(SharedServer s : servers.values()) {
			try {
				s.uploadPicture(album.getName(), name, data);
				found = true;
				albumUpdate.put(album.getName(), Long.MIN_VALUE);
			} catch (InexistentAlbumException_Exception | ExistentPictureException_Exception | IOException_Exception e) {
				
			}
		}
		if(found)
			return new SharedPicture(name);
		else return null;
	}

	/**
	 * Delete a picture from an album.
	 * On error this method should return false.
	 */
	@Override 
	public boolean deletePicture(Album album, Picture picture) {
		
		boolean found = false;
		for(SharedServer s : servers.values()) {
			try {
				s.deletePicture(album.getName(), picture.getName());
				found = true;
				albumUpdate.put(album.getName(), Long.MIN_VALUE);
			} catch (InexistentAlbumException_Exception | IOException_Exception | InexistentPictureException_Exception e) {
				
			}
		}
		return found;
	}

	
	/**
	 * Represents a shared album.
	 */
	static class SharedAlbum implements GalleryContentProvider.Album {
		final String name;

		SharedAlbum(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
	}

	/**
	 * Represents a shared picture.
	 */
	static class SharedPicture implements GalleryContentProvider.Picture {
		final String name;

		SharedPicture(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
	}
}
