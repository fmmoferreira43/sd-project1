
package sd.tp1.clt.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "InexistentAlbumException", targetNamespace = "http://srv.tp1.sd/")
public class InexistentAlbumException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private InexistentAlbumException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public InexistentAlbumException_Exception(String message, InexistentAlbumException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public InexistentAlbumException_Exception(String message, InexistentAlbumException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: sd.tp1.clt.ws.InexistentAlbumException
     */
    public InexistentAlbumException getFaultInfo() {
        return faultInfo;
    }

}
