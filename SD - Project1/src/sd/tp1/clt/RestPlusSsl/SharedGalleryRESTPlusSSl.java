package sd.tp1.clt.RestPlusSsl;

import javafx.application.Application;
import javafx.stage.Stage;
import sd.tp1.clt.RestPlus.SharedGalleryContentProviderRESTPlus;
import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.impl.GalleryWindow;

/*
 * Launches the local shared gallery application.
 */
public class SharedGalleryRESTPlusSSl extends Application {

	GalleryWindow window;
	
	public SharedGalleryRESTPlusSSl() throws Exception {
		window = new GalleryWindow(new SharedGalleryContentProviderRESTPlusSSL());
	}	
	
	
    public static void main(String[] args){
        launch(args);
    }
    
	@Override
	public void start(Stage primaryStage) throws Exception {
		window.start(primaryStage);
	}
}
