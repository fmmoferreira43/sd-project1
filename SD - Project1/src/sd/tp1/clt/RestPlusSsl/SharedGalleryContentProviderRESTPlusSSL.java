package sd.tp1.clt.RestPlusSsl;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.URL;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

import sd.tp1.clt.Messaging.ClientMessaging;
//import sd.tp1.clt.RestPlus.SharedGalleryContentProviderRESTPlusSSL.SharedAlbum;
//import sd.tp1.clt.RestPlus.SharedGalleryContentProviderRESTPlusSSL.SharedPicture;
import sd.tp1.clt.ssl.ws.ExistentPictureException_Exception;
import sd.tp1.clt.ssl.ws.ExistingAlbumException_Exception;
import sd.tp1.clt.ssl.ws.IOException_Exception;
import sd.tp1.clt.ssl.ws.InexistentAlbumException_Exception;
import sd.tp1.clt.ssl.ws.InexistentPictureException_Exception;
import sd.tp1.clt.ssl.ws.SharedServerSSl;
import sd.tp1.clt.ssl.ws.SharedServerSSlService;
import sd.tp1.clt.ws.SharedServer;
import sd.tp1.clt.ws.SharedServerService;
import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.Gui;
import sd.tp1.srv.Messaging.ServerMessaging;

public class SharedGalleryContentProviderRESTPlusSSL implements GalleryContentProvider {
	
	Map<String, SharedServer> wsdlServers;
	Map<String, SharedServerSSl> sslServers;
	Map<String, WebTarget> restServers;
	Map<String, Long> serverResponse;


	//cache stuff
	private File cache;
	private long galleryUpdate;
	private Map<String, Long> pictureUpdate;
	private Map<String, List<Picture>> albumContents;
	private Map<String, Long> albumUpdate;
	
	private Gui gui;

		
	//public static final long UPDATE_INTERVAL = 5000;
	public static final int ATEMPTS = 100;
	public static final String MULTICAST_GROUP = "228.5.6.7";
	public static final int MULTICAST_PORT = 6789;
	public static final int TIMEOUT = 1000;
	public static final int BUFFER_SIZE = 65536;
	public static final String URL = "%s://%s:%s/Gallery";
	public static final long REQUEST_INTERVAL = 2500;
	public static final long DELETE_SERVER = 10000;

	private static final long UPDATE_ALBUM = 5000;
	private static final long UPDATE_PICTURE = 15000;
	private static final long UPDATE_CONTENT = 30000;




public  SharedGalleryContentProviderRESTPlusSSL() throws NoSuchAlgorithmException, KeyManagementException {
			//multiserver stuff
			wsdlServers = new ConcurrentHashMap<String, SharedServer>(); //here we store the WSDL servers
			sslServers = new ConcurrentHashMap<String, SharedServerSSl>();
			restServers = new ConcurrentHashMap<String, WebTarget>();    //here we store the REST servers
			serverResponse = new ConcurrentHashMap<String, Long>();      //here we store the last time a server replied to a "Looking" query
			
			//cache stuff
			galleryUpdate = 0;
			pictureUpdate = new HashMap<String, Long>();
			albumContents = new HashMap<String, List<Picture>>();
			albumUpdate = new HashMap<String, Long>();
			cache = new File(".", "Cache");
			if(!cache.exists())
				cache.mkdir();
			
			
			
			//start server listener
			Thread serverListener = new Thread(() -> {
				MulticastSocket cast = null;
				InetAddress group = null;
				long lastRequest = Long.MIN_VALUE;
				try {
					group = InetAddress.getByName(MULTICAST_GROUP);
					cast = new MulticastSocket(MULTICAST_PORT);
					cast.setSoTimeout(TIMEOUT);
					cast.joinGroup(group);
				} catch (Exception e1) {
					System.err.println("Server discovery has quit. " + e1.getMessage());
					return;
				}
				
				
				while(true) {
					
					
					if(lastRequest + REQUEST_INTERVAL < System.currentTimeMillis()) {
						lastRequest = System.currentTimeMillis();
						System.err.println("Listening for servers...");
						byte[] b = String.format("%s", "Looking").getBytes();
						DatagramPacket p = new DatagramPacket(b, b.length, group, MULTICAST_PORT);
						try {
							cast.send(p);
						} catch (Exception e) {
							System.err.println(e.getMessage());
							continue;
						}
						
					} 
					
					byte[] buf = new byte[BUFFER_SIZE];
					DatagramPacket r = new DatagramPacket(buf, buf.length);
					try {
						cast.receive(r);
						
						
					
						String val = new String(r.getData(), 0, r.getLength());
						String[] message = val.split(" ");
						System.err.println("Server type: " + val);
						if(message[0].equals("WSDL")) {
							
							if(message[1].equals("SSL")) {
								String url = String.format(URL, "https", r.getAddress().getHostAddress(), message[2]);
								if(!sslServers.containsKey(url)) {
									
									SSLContext sc = SSLContext.getInstance("TLSv1");
									TrustManager[] trustAllCerts = { new InsecureTrustManager()};
									sc.init(null, trustAllCerts, new java.security.SecureRandom());
									HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
									HttpsURLConnection.setDefaultHostnameVerifier( new InsecureHostNameVerifier());
									
									SharedServerSSlService serv;
									System.out.println(url);
									serv = new SharedServerSSlService(new URL(url));
									SharedServerSSl s = serv.getSharedServerSSlPort();
									sslServers.put(url, s);
									System.err.println("Connected with server @ " + url);
								} else System.err.println("Already connected with " + url);								
								serverResponse.put(url, System.currentTimeMillis());
							} else {
								String url = String.format(URL, "http", r.getAddress().getHostAddress(), message[1]);
								if(!wsdlServers.containsKey(url)) {
									SharedServerService serv = new SharedServerService(new URL(url));
									SharedServer s = serv.getSharedServerPort();
									wsdlServers.put(url, s);
								}
								serverResponse.put(url, System.currentTimeMillis());
							}
							
							
						} else if(message[0].equals("REST")) {
							if(message[1].equals("SSL")) {
								String url = String.format(URL, "https", r.getAddress().getHostAddress(), message[2]);
								if(!restServers.containsKey(url)) {
									
									SSLContext sc = SSLContext.getInstance("TLSv1");
									TrustManager[] trustAllCerts = { new InsecureTrustManager()};
									sc.init(null, trustAllCerts, new java.security.SecureRandom());
									
									ClientConfig config = new ClientConfig();
									Client client = ClientBuilder.newBuilder()
											.hostnameVerifier(new InsecureHostNameVerifier())
											.sslContext(sc)
											.build();
									WebTarget target = client.target(url);
									restServers.put(url, target);
									System.err.println("Connected with server @ " + url);
								} else System.err.println("Already connected with " + url);
								serverResponse.put(url, System.currentTimeMillis());
							} else {
								String url = String.format(URL, "http", r.getAddress().getHostAddress(), message[1]);
								ClientConfig config = new ClientConfig();
								Client client = ClientBuilder.newClient(config);
								WebTarget target = client.target(url);
								restServers.put(url, target);
								serverResponse.put(url, System.currentTimeMillis());
							}
						} else System.err.println("Unsuported server type: " + val);
						
					}  catch (java.net.SocketTimeoutException time) {
						//System.err.println("Timed out..");
					}  catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				
				
			});
			serverListener.start();
			
			//start server remover
			Thread removeServer = new Thread(() -> {
				
				for(Entry<String, Long> e : serverResponse.entrySet()) {
					Long time = e.getValue();
					if(time < System.currentTimeMillis() - DELETE_SERVER) {					
						serverResponse.remove(e.getKey());
						if(wsdlServers.containsKey(e.getKey()))
							wsdlServers.remove(e.getKey());
						if(restServers.containsKey(e.getKey()))
							restServers.remove(e.getKey());
						if(sslServers.containsKey(e.getKey()))
							sslServers.remove(e.getKey());
						try {
							wait(1000);
						} catch (Exception e1) {
						}
					}
				}
				
			});
			removeServer.start();
			
				
			
			while(wsdlServers.isEmpty() && restServers.isEmpty() && sslServers.isEmpty()) {
				
			}
			
			ClientMessaging mess = new ClientMessaging(this.gui);
			mess.start();
			
}


public void register(Gui gui) {
	if( this.gui == null ) {
		this.gui = gui;
	}
}


public List<Album> getListOfAlbums() {
	
	System.out.println("Getting album list.");
	
	List<Album> lst = new ArrayList<Album>();
	if(System.currentTimeMillis() < this.galleryUpdate + UPDATE_ALBUM) {
		for(String s : cache.list())
			lst.add(new SharedAlbum(s));
	} else {
	
		this.galleryUpdate = System.currentTimeMillis();		
		for(SharedServerSSl s : sslServers.values())
			for(String a: s.getAlbums()){
				if(!lst.contains(a))
					lst.add(new SharedAlbum(a));
			}
		
		for(SharedServer s : wsdlServers.values()) {
			for(String a : s.getAlbums()) {
				if(!lst.contains(a))
					lst.add(new SharedAlbum(a));
			}
		}
		
		for(WebTarget t : restServers.values()){
			List<String> l = null;
			Response r = t.request(MediaType.APPLICATION_JSON).get();
			if(r.getStatus() != Status.OK.getStatusCode()) {
				System.out.println(r.getStatus() + " -> " + Status.OK.getStatusCode());
				return null;
			}
			
			l = r.readEntity(ArrayList.class);
			for(String s : l)
				if(!lst.contains(s))
					lst.add(new SharedAlbum(s));
		}
		
	}
	
	
	return lst;
}

public List<Picture> getListOfPictures(Album album) {
	// TODO: obtain remote information 
	
	if(this.albumUpdate.containsKey(album.getName()) 
			&& albumUpdate.get(album.getName()) + UPDATE_CONTENT > System.currentTimeMillis()) {
		return this.albumContents.get(album.getName());
	}
	
	List<Picture> lst = new ArrayList<Picture>();
	
		for(SharedServerSSl s : sslServers.values())
			try {
				for(String a: s.getListOfPictures(album.getName())){
					if(!lst.contains(a))
						lst.add( new SharedPicture(a));
				}
			} catch (IOException_Exception | InexistentAlbumException_Exception e) {
				continue;
			}
	
	for(WebTarget t : restServers.values()){
		List<String> l = null;
		Response r = t.path("/ListPictures/" + album.getName()).request().accept(MediaType.APPLICATION_JSON).get();
		if(r.getStatus() != Status.OK.getStatusCode()) return null;		
		l = r.readEntity(ArrayList.class);
		lst = new ArrayList<Picture>();
		for(String s : l)
			if(!lst.contains(s))
				lst.add(new SharedPicture(s));
	}
	
	albumContents.put(album.getName(), lst);
	albumUpdate.put(album.getName(), System.currentTimeMillis());
	
	return lst;
}

/**
 * Returns the contents of picture in album.
 * On error this method should return null.
 */

public byte[] getPictureData(Album album, Picture picture) {
	
	String key = album.getName() + "/" + picture.getName();
	
	System.out.println("Fetching " + key);
	
	File a = new File(cache.toPath().toString(), album.getName());
	if(a.exists() && this.pictureUpdate.containsKey(key) && pictureUpdate.get(key) + UPDATE_PICTURE < System.currentTimeMillis()) {
		File p = new File(a.toPath().toString(), picture.getName());
		if(p.exists()) {
			try {
				return Files.readAllBytes(p.toPath());
			} catch (IOException e) {
				//if it fails, we'll try to fetch it from the server
			}
		}
	
	} else a.mkdir();
	
	System.err.println("Not found @ Cache");
	
	for(SharedServer s : wsdlServers.values()) {
		try {
			byte[] data = s.getPictureData(album.getName(), picture.getName());
			File p = new File(a.toPath().toString(), picture.getName());
			OutputStream out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
			out.write(data);
			pictureUpdate.put(key, System.currentTimeMillis());
			return data;
		}  catch (IOException e) {
			continue;
		} catch (sd.tp1.clt.ws.IOException_Exception e) {
			continue;
		} catch (sd.tp1.clt.ws.InexistentAlbumException_Exception e) {
			
			continue;
		} catch (sd.tp1.clt.ws.InexistentPictureException_Exception e) {
			continue;
		}
		
	}
	
	System.err.println("Not found @ WSDL");
	
	for(SharedServerSSl s : sslServers.values()) {		
			
		try {
			byte[] data = s.getPictureData(album.getName(), picture.getName());
			File p = new File(a.toPath().toString(), picture.getName());
			OutputStream out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
			out.write(data);
			pictureUpdate.put(key, System.currentTimeMillis());
			return data;
		} catch (IOException_Exception | InexistentAlbumException_Exception
				| InexistentPictureException_Exception e) {
			
			continue;
		} catch (IOException e) {
			continue;
		}			
		
	}

	System.err.println("Not found @ SSL");
	
	for(WebTarget t : restServers.values()){
		Response r = t.path("/get/"+ album.getName() +"/" + picture.getName()).request().get();
		if(r.getStatus() != Status.OK.getStatusCode()) continue;
		byte[] b = r.readEntity(byte[].class);
		File p = new File(a.toPath().toString(), picture.getName());
		OutputStream out;
		try {
			out = Files.newOutputStream(p.toPath(), CREATE, APPEND);
			out.write(b);
		} catch (IOException e) {
			continue;
		}
		pictureUpdate.put(key, System.currentTimeMillis());
	    return b;
	}
	
	System.err.println("Not found @ REST");
				//System.out.println("Data size: " + data.length);
	return null;
}



/**
 * Create a new album.
 * On error this method should return null.
 */
public Album createAlbum(String name) {
	try {
			
		Random generator = new Random();
		Object[] values = serverResponse.keySet().toArray();
		String url = (String) values[generator.nextInt(values.length)];
		
		if(wsdlServers.containsKey(url)) {
			wsdlServers.get(url).createAlbum(name);
		} else if (sslServers.containsKey(url)) {
			sslServers.get(url).createAlbum(name);
		} else if (restServers.containsKey(url)) {
			WebTarget t = restServers.get(url);
			Response r = t.path("/CreateAlbum").request().post(Entity.entity(name, MediaType.APPLICATION_OCTET_STREAM));
			if(r.getStatus() != Status.OK.getStatusCode()) return null;
			this.galleryUpdate = Long.MIN_VALUE;
		}
		this.galleryUpdate = Long.MIN_VALUE;
	} catch (sd.tp1.clt.ws.ExistingAlbumException_Exception | sd.tp1.clt.ws.IOException_Exception | ExistingAlbumException_Exception | IOException_Exception e) {
		return null;
	}

	return new SharedAlbum(name);
	
}

/**
 * Delete an existing album.
 */
public void deleteAlbum(Album album) {
	
	for(SharedServer s : wsdlServers.values()) {
		try {
			s.deleteAlbum(album.getName());
		} catch (sd.tp1.clt.ws.IOException_Exception | sd.tp1.clt.ws.InexistentAlbumException_Exception e) {
			continue;
		}
	}
	
	for(SharedServerSSl s : sslServers.values()) {
		try {
			s.deleteAlbum(album.getName());
		} catch (IOException_Exception | InexistentAlbumException_Exception e) {
			continue;
		}
	}
	
	for(WebTarget t : restServers.values())
		t.path("/DeleteAlbum/" + album.getName()).request().delete();
	this.galleryUpdate = Long.MIN_VALUE;
}

/**
 * Add a new picture to an album.
 * On error this method should return null.
 */
public Picture uploadPicture(Album album, String name, byte[] data) {
	// TODO: contact servers to add picture name with contents data
	
	System.out.println("Uploading picture.");

	for(SharedServerSSl s : sslServers.values()) {
		try {
			s.uploadPicture(album.getName(), name, data);
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			return new SharedPicture(name);
		} catch (InexistentAlbumException_Exception | ExistentPictureException_Exception | IOException_Exception e) {
			continue;
		}
	}
	
	for(SharedServer s : wsdlServers.values()) {
		try {
			s.uploadPicture(album.getName(), name, data);
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			return new SharedPicture(name);
		} catch (sd.tp1.clt.ws.ExistentPictureException_Exception | sd.tp1.clt.ws.IOException_Exception | sd.tp1.clt.ws.InexistentAlbumException_Exception e) {
			continue;
		}
	}
	
	for(WebTarget t : restServers.values()){
		System.out.println("Uploading " + name);
		Response r = t.path("/Upload/" + album.getName() + "/" + name).request().post(Entity.entity(data, MediaType.APPLICATION_OCTET_STREAM));
		System.out.println("Response status: " + r.getStatus());
		if(r.getStatus() != Status.OK.getStatusCode())	{			
			continue; 
		} else {
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			return new SharedPicture(name);
		}
	}
	
	return null;
	

}

/**
 * Delete a picture from an album.
 * On error this method should return false.
 */
public boolean deletePicture(Album album, Picture picture) {
	

	for(SharedServerSSl s : sslServers.values()) {
		try {
			s.deletePicture(album.getName(), picture.getName());
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			//return true;
		} catch (InexistentAlbumException_Exception | IOException_Exception | InexistentPictureException_Exception e) {
			continue;
		}
	}
	
	for(SharedServer s : wsdlServers.values()) {
		try {
			s.deletePicture(album.getName(), picture.getName());
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			//return true;
		} catch (sd.tp1.clt.ws.IOException_Exception | sd.tp1.clt.ws.InexistentAlbumException_Exception
				| sd.tp1.clt.ws.InexistentPictureException_Exception e) {
			continue;
		}
	}
	
	for(WebTarget t : restServers.values()){
		Response r = t.path("/DeletePicture/" + album.getName() + "/" + picture.getName()).request().delete();
		if(r.getStatus() == Status.OK.getStatusCode()) {
			albumUpdate.put(album.getName(), Long.MIN_VALUE);
			//return true;
		}
	}
	
	return false;
}


/**
 * Represents a shared album.
 */
static class SharedAlbum implements GalleryContentProvider.Album {
	final String name;

	SharedAlbum(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof String) {
			return ((String)obj).equals(this.name);
		}
		
		if(obj instanceof SharedAlbum) {
			return ((SharedAlbum) obj).getName().equals(this.name);
		}
		return false;
		
	}
}

/**
 * Represents a shared picture.
 */
static class SharedPicture implements GalleryContentProvider.Picture {
	final String name;

	SharedPicture(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof String) {
			return ((String)obj).equals(this.name);
		}
		
		if(obj instanceof SharedPicture) {
			return ((SharedPicture) obj).getName().equals(this.name);
		}
		return false;
		
	}
	
	
}

//SSL Fuctions
static public class InsecureHostNameVerifier implements HostnameVerifier {

	@Override
	public boolean verify(String arg0, SSLSession arg1) {
		// TODO Auto-generated method stub
		return true;
	}
}

static public class InsecureTrustManager implements X509TrustManager {

	@Override
	public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		// TODO Auto-generated method stub
		Arrays.asList( arg0 ).forEach( i -> {
		System.err.println("from: " + i.getNotBefore() + "to: " + i.getNotAfter() );
		});
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		// TODO Auto-generated method stub
		return new X509Certificate[0];
	}
	
}

}
