package sd.tp1.clt.Messaging;


import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import sd.tp1.gui.GalleryContentProvider;
import sd.tp1.gui.Gui;


public class ClientMessaging extends Thread implements Runnable {
	
	private Properties props;
	private KafkaConsumer<String, String> consumer;
	private Gui	client;
	
	public ClientMessaging(Gui client) {
		this.client = client;
		props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "consumer-tutorial" + System.nanoTime());
		props.put("key.deserializer", StringDeserializer.class.getName());
		props.put("value.deserializer", StringDeserializer.class.getName());
		consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("updated"));
	}
	
	
	public void run() {
		
		
		while(true) {
			ConsumerRecords<String, String> records = consumer.poll(1000);
		    records.forEach( r -> {			    	
		    	System.err.println( r.topic() + "/" + r.value());
		    	
		    	switch(r.topic()) {
		    		case "updated":
		    				client.updateAlbum(new SharedAlbum(r.value()));    		
		    			break;
		    	}
		    	
		    });			
			
		}
		
		
	}
	
	/**
	 * Represents a shared album.
	 */
	static class SharedAlbum implements GalleryContentProvider.Album {
		final String name;

		SharedAlbum(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof String) {
				return ((String)obj).equals(this.name);
			}
			
			if(obj instanceof SharedAlbum) {
				return ((SharedAlbum) obj).getName().equals(this.name);
			}
			return false;
			
		}
	}

}
